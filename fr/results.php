<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7 no-js" lang="en-US">
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8 no-js" lang="en-US">
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html lang="en" class="no-js">
<head>
	<!-- Basic need -->
	<title>AfrikEveil</title>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="">
	<link rel="profile" href="#">
	<?php include_once("librairies/header.php");?>
</head>

<body>
<?php include_once("librairies/menu.php");?>
<div class="hero">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1>Resultats de la recherche</h1>
				<img src="../commun/images/uploads/hero-line.png" alt="">
				<ul class="breadcumb">
					<li><a href="#">ACCUEIL</a></li>
					<li><span>/</span>Recherche</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="bloglistpost-v1 blogv3 blog servicesingle">
	<div class="container">
		<blockquote>Resultats de la recherche</blockquote>
		<div class="row">
			<div class="col-md-9 col-sm-12 col-xs-12">
				<div class="blogpost-v1">
					<div class="blog-it normal">
						<div class="post-thumbnail">
							<a href="#"><img class="post-img" src="../commun/images/uploads/blog-21.jpg" alt="thumb1"></a>
						</div>
						<div class="blog-it-left">
							<div class="row">
								<div class="col-md-2 col-sm-3 col-xs-12">
									<div class="date">
										<h1>09</h1>
										<img src="../commun/images/uploads/white-line.png" alt="">
										<p>DEC</p>
									</div>
								</div>
								<div class="col-md-10 col-sm-9 col-xs-12">
									<div class="inner-ct">
										<h2><a href="blogsingle.html">Développement de l’entrepreneuriat à finalité sociale...</a></h2>
										<div class="date-inner">
											<span><i class="ion-android-person"></i><em>By</em> Afrik Eveil</span>
											<span>|</span>
											<span><i class="fa fa-commenting" aria-hidden="true"></i><em>03</em> Comments</span>
											<span><i class="fa fa-calendar" aria-hidden="true"></i><em>February</em> 9th, 2017</span>
										</div>
										<img class="divide2" src="../commun/images/uploads/blogline.png" alt="">
										<p>La contribution positive des entreprises aux économies nationale et mondiale est un fait bien connu. Leur capacité à soutenir la croissance, créer des emplois et générer des revenus montre combien elles sont indispensables. Mais que font concrètement les structures d’accompagnement des pays..</p>
										<a class="read" href="#">lire Plus</a>
									</div>
								</div>
							</div>
						</div>
					</div>
									<div class="blog-it normal">
						<div class="post-thumbnail">
							<a href="#"><img class="post-img" src="../commun/images/uploads/blog-21.jpg" alt="thumb1"></a>
						</div>
						<div class="blog-it-left">
							<div class="row">
								<div class="col-md-2 col-sm-3 col-xs-12">
									<div class="date">
										<h1>09</h1>
										<img src="../commun/images/uploads/white-line.png" alt="">
										<p>DEC</p>
									</div>
								</div>
								<div class="col-md-10 col-sm-9 col-xs-12">
									<div class="inner-ct">
										<h2><a href="blogsingle.html">Développement de l’entrepreneuriat à finalité sociale...</a></h2>
										<div class="date-inner">
											<span><i class="ion-android-person"></i><em>By</em> Afrik Eveil</span>
											<span>|</span>
											<span><i class="fa fa-commenting" aria-hidden="true"></i><em>03</em> Comments</span>
											<span><i class="fa fa-calendar" aria-hidden="true"></i><em>February</em> 9th, 2017</span>
										</div>
										<img class="divide2" src="../commun/images/uploads/blogline.png" alt="">
										<p>La contribution positive des entreprises aux économies nationale et mondiale est un fait bien connu. Leur capacité à soutenir la croissance, créer des emplois et générer des revenus montre combien elles sont indispensables. Mais que font concrètement les structures d’accompagnement des pays..</p>
										<a class="read" href="#">lire Plus</a>
									</div>
								</div>
							</div>
						</div>
					</div>
									<div class="blog-it normal">
						<div class="post-thumbnail">
							<a href="#"><img class="post-img" src="../commun/images/uploads/blog-21.jpg" alt="thumb1"></a>
						</div>
						<div class="blog-it-left">
							<div class="row">
								<div class="col-md-2 col-sm-3 col-xs-12">
									<div class="date">
										<h1>09</h1>
										<img src="../commun/images/uploads/white-line.png" alt="">
										<p>DEC</p>
									</div>
								</div>
								<div class="col-md-10 col-sm-9 col-xs-12">
									<div class="inner-ct">
										<h2><a href="blogsingle.html">Développement de l’entrepreneuriat à finalité sociale...</a></h2>
										<div class="date-inner">
											<span><i class="ion-android-person"></i><em>By</em> Afrik Eveil</span>
											<span>|</span>
											<span><i class="fa fa-commenting" aria-hidden="true"></i><em>03</em> Comments</span>
											<span><i class="fa fa-calendar" aria-hidden="true"></i><em>February</em> 9th, 2017</span>
										</div>
										<img class="divide2" src="../commun/images/uploads/blogline.png" alt="">
										<p>La contribution positive des entreprises aux économies nationale et mondiale est un fait bien connu. Leur capacité à soutenir la croissance, créer des emplois et générer des revenus montre combien elles sont indispensables. Mais que font concrètement les structures d’accompagnement des pays..</p>
										<a class="read" href="#">lire Plus</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<ul class="pagination">
						<p>page: </p>
						<li class="num active"><a href="#">01</a></li>
						<li class="num"><a href="#">02</a></li>
						<li class="num2"><a href="#">...</a></li>
						<li class="num"><a href="#">30</a></li>
						<li class="icon"><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
					</ul>
				</div>
			</div>
			<div class="col-md-3 col-sm-12 col-xs-12">
				<div class="bg-sidebar sidebar">
					<div class="search">
						<input class="search-input" type="text" placeholder="search post here">
						<i class="fa fa-search" aria-hidden="true"></i>
					</div>
					<div class="sb-it">
						<div class="sb-title">
							<h2>Feature Services</h2>
							<img src="../commun/images/uploads/line-title2.png" alt="">
						</div>
						<div class="sb-content">
							<p class="sv"><a href="#">Garden Care</a></p>
							<p class="sv"><a href="#">Lawn Mowing</a></p>
							<p class="sv"><a href="#">Snow Removal</a></p>
							<p class="sv"><a href="#">Pest Control</a></p>
							<p class="sv"><a href="#">Other Sevices</a></p>
						</div>
					</div>

					<div class="pp-posts sb-it">
						<div class="sb-title">
							<h2>Recent Posts</h2>
							<img src="../commun/images/uploads/line-title2.png" alt="">
						</div>
						<div class="pp-post-it">
							<div class="row">
								<div class="col-md-4 col-sm-3 col-xs-12">
									<a href="#"><img src="../commun/images/uploads/post1.png" alt="post1"></a>
								</div>
								<div class="col-md-8 col-sm-9 col-xs-12">
									<div class="pp-infor">
										<h5><a href="blogsingle.html">Fresh fruits from the  White House</a></h5>
										<div class="date-pp">
											<span>Octorber 5th, 2016</span>
										</div>
									</div>
								</div>
							</div>
						</div>
						<img class="dot-div" src="../commun/images/uploads/dotline.png" alt="">
						<div class="pp-post-it">
							<div class="row">
								<div class="col-md-4 col-sm-3 col-xs-12">
									<a href="#"><img src="../commun/images/uploads/post2.png" alt="post1"></a>
								</div>
								<div class="col-md-8 col-sm-9 col-xs-12">
									<div class="pp-infor">
										<h5><a href="blogsingle.html">Should We Rewash Pre-washed Greens? </a></h5>
										<div class="date-pp">
											<span>Octorber 5th, 2016</span>
										</div>
									</div>
								</div>
							</div>
						</div>
						<img class="dot-div" src="../commun/images/uploads/dotline.png" alt="">
						<div class="pp-post-it">
							<div class="row">
								<div class="col-md-4 col-sm-3 col-xs-12">
									<a href="#"><img src="../commun/images/uploads/post3.png" alt="post1"></a>
								</div>
								<div class="col-md-8 col-sm-9 col-xs-12">
									<div class="pp-infor">
										<h5><a href="blogsingle.html">Health Benefits of a Raw Food</a></h5>
										<div class="date-pp">
											<span>Octorber 5th, 2016</span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="sb-it archive">
						<div class="sb-title">
							<h2>Archive</h2>
							<img src="../commun/images/uploads/line-title2.png" alt="">
						</div>
						<ul class="sb-content">
							<li class="sv"><a href="#"> May 2015 (4)</a></li>
							<li class="sv"><a href="#">January 2015 (5)</a></li>
							<li class="sv"><a href="#"> December 2014 (6)</a></li>
							<li class="sv"><a href="#"> October 2014 (8)</a></li>
						</ul>
					</div>
					<div class="searchbytag sb-it">
						<div class="sb-title">
							<h2>tags</h2>
							<img src="../commun/images/uploads/line-title2.png" alt="">
						</div>
						<ul class="tags">
							<li><a href="galleryv1.html">WP</a></li>
							<li><a href="galleryv1.html">Haintheme</a></li>
							<li><a href="galleryv1.html">Garden</a></li>
							<li><a href="galleryv1.html">Love</a></li>
							<li><a href="galleryv1.html">World</a></li>
							<li><a href="galleryv1.html">Lawn</a></li>
							<li><a href="galleryv1.html">Care</a></li>
							<li><a href="galleryv1.html">Snow</a></li>
							<li><a href="galleryv1.html">Snow removal</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include_once("librairies/footer.php");?>
<?php include_once("librairies/scripts.php");?>
</body>
</html>