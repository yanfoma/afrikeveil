<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7 no-js" lang="en-US">
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8 no-js" lang="en-US">
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html lang="en" class="no-js">
<head>
	<!-- Basic need -->
	<title>NOTRE ORGANISATION | Fondation Afrik Eveil</title>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="">
	<link rel="profile" href="#">
	<?php include_once("librairies/header.php");?>
	<style type="text/css">
		.tab p {
			text-align: justify;
		}
	</style>
</head>

<body>
<?php include_once("librairies/menu.php");?>
<div class="hero">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1>Notre Organisation</h1>
				<img src="../commun/images/uploads/hero-line.png" alt="">
				<ul class="breadcumb">
					<li><a href="index.php">ACCUEIL</a></li>
					<li><span>/</span>QUI NOUS SOMMES</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="intro clientsv3">
	<div class="container">
		<div class="intro-ct">
			<div class="row">
				<div class="col-md-6 col-sm-12 col-xs-12">
					<div id="owl-intro">
						<div class="intro-it item">
							<img src="../commun/images/uploads/intro-slider.png" alt="">
						</div>
					</div>
				</div>
				<div class="col-md-6 col-sm-12 col-xs-12">
					<img class="quote-ico" src="../commun/images/uploads/quote-ico.png" alt="">
					<div class="clientv3-it">
						<p>Our mission is to connect academia and industry while supporting young entrepreneurs in starting and scaling-up their businesses to reduce unemployment among youth.</p>
						<a href="#">---  Damien Somé - Chairman/ Founder</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="projectv3">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-12 col-xs-12">
				<div class="video">
					<img  class="thumbnail" src="../commun/images/uploads/projectv3-img.png" alt="">
					<a class="html5lightbox thm-btn" title="Introduction DE AFRIK EVEIL" href="https://youtu.be/mObSx7bMNTE"><img class="btn hvr-grow" src="../commun/images/uploads/playbtn1.png" alt=""></a>
				</div>
			</div>
			<div class="col-md-6 col-sm-12 col-xs-12">
				<div class="project-left">
					<div class="heading-it">
						<h1>La Foundation Afrik Eveil</h1>
						<img src="../commun/images/uploads/line-title2.png" alt="">
						<p style="text-align:justify;">
							Afrik Eveil est une organisation à but non lucratif, créée et gérée par des professionnels expérimentés qui se moblisent en faveur de l'entrepreneuriat comme moyen efficace de promotion de l’employabilité des jeunes en Afrique.
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="aboutservice">
	<div class="container">
		<!--div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="heading-it">
					<img src="../commun/images/uploads/line-title2.png" alt="">
					<p>Nous sommes un laboratoire d’idées et d’actions dédié à l’entrepreneuriat en Afrique. Basée à Ouagadougou, notre organisation est également représentée à Accra, Genève, Paris et Taipei.</p>
				</div>
			</div>
		</div-->
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="tabs">
					<ul class="tab-links">
						<li class="active"><a href="#tab1"> MISSION</a></li>
						<li><a href="#tab2">VISION</a></li>
						<li><a href="#tab3">APPROCHE</a></li>
					</ul>
				    <div class="tab-content">
				        <div id="tab1" class="tab active">
				            <div class="row">
				            	<div class="col-md-4 col-sm-12 col-xs-12">
				            		<img src="../commun/images/uploads/mission.png" alt="">
				            	</div>
				            	<div class="col-md-8 col-sm-12 col-xs-12">
				            		<h2><a href="#"></a></h2>
				            		<p>Notre mission est de connecter le milieux universitaire et l’industrie en aidant des jeunes entrepreneurs à créer et à développer leurs entreprises afin de réduire le chômage.
									</p>
				            	</div>
				            </div>
				        </div>
				        <div id="tab2" class="tab ">
				           <div class="row">
				            	<div class="col-md-4 col-sm-12 col-xs-12">
				            		<img src="../commun/images/uploads/vision.png" alt="">
				            	</div>
				            	<div class="col-md-8 col-sm-12 col-sm-12">
				            		<h2><a href="#"></a></h2>
				            		<p>Notre vision est de devenir l’une des principales organisations, fondée sur le savoir et experte dans le soutien à l’innovation et l’accompagnement de jeunes entrepreneurs en Afrique.</p>
				            	</div>
				            </div>
				        </div>
				        <div id="tab3" class="tab">
				        	<div class="row">
				            	<div class="col-md-4 col-sm-12 col-xs-12">
				            		<img src="../commun/images/uploads/approche.png" alt="">
				            	</div>
				            	<div class="col-md-8 col-sm-12 col-sm-12">
				            		<h2><a href="#"></a></h2>
				            		<p>Nous combinons la recherche appliquée à des analyses approfondies et à  l’intelligence décisionnelles pour combler le vide de données, de recherches de qualité et de conseils stratégiques dans le domaine de l’ entrepreneuriat en Afrique.
									</p>
				            	</div>
				            </div>
			       	 	</div>
				    </div>
				</div>
			</div>
		</div>

	</div>
</div>
<br><br><br>
<div class="contactv1">
	<div class="container contact-ct">
		<div class="row">
			<div class="col-md-9 col-sm-8 col-xs-12">
				<h1>Notre Brochure Pour Plus d'informations.</h1>
			</div>
			<div class="col-md-3 col-sm-4 col-xs-12">
				<div class="contact-bt">
					<a href="../commun/documents/Trifold-AFRIKEVEIL.pdf" target="_blank" class="readmore2">Telecharger</a>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include_once("librairies/footer.php");?>
<?php include_once("librairies/scripts.php");?>
    <!-- Html 5 light box script-->
<script src="../commun/js/html5lightbox/html5lightbox.js"></script>
</body>
</html>