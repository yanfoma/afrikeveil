<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7 no-js" lang="en-US">
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8 no-js" lang="en-US">
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html lang="en" class="no-js">
<head>
	<!-- Basic need -->
	<title>Notre Projet | Fondation Afrik Eveil</title>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="">
	<link rel="profile" href="#">
	<?php include_once("librairies/header.php");?>
</head>

<body>
<?php include_once("librairies/menu.php");?>
<div class="hero">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1>Notre Projet  </h1>
				<img src="../commun/images/uploads/hero-line.png" alt="">
				<ul class="breadcumb">
					<li><a href="index.php">ACCEUIL</a></li>
					<li><span>/</span> Notre Projet</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="servicesv3">
	<div class="container">
		<div class="row">
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="heading-it">
					<h1>Notre Projet</h1>
					<img src="../commun/images/uploads/line-title2.png" alt="">
					<p>Veuillez défiler<br> pour tout voir.</p>
				</div>
			</div>
			<div class="col-md-9 col-sm-6 col-xs-12">
				<div id="owl-servicev3">
					<div class="sv-item item it1">
						<div class="hover-inner">
							<p>Afrik Eveil  est une organisation de jeunes professionnels et étudiants Burkinabè de la diaspora et de l’intérieur du pays qui ont volontairement choisi de s’engager dans  la promotion de l’entrepreneuriat social comme moyen de lutte contre le chômage des jeunes diplômés au Burkina Faso.</p>
						</div>
					</div>
					<div class="sv-item item it2">
						<div class="hover-inner">
							<p>Notre mission est d’inculquer aux jeunes diplômés l’esprit d’entreprise d’une part et d’autre part d’engager davantage la diaspora Burkinabè dans les projets de développement de la mère-patrie.</p>
						</div>
					</div>
					<div class="sv-item item it2">
						<div class="hover-inner">
							<p>Nous œuvrons aussi à tisser un vaste réseau de contacts (Burkina, Taïwan, Etats-Unis, Canada, France, Suisse, Maroc, Italie…) afin de donner les opportunités à ceux qui veulent relever les défis du moment.</p>
						</div>
					</div>
					<div class="sv-item item it2">
						<div class="hover-inner">
							<p>Notre vision est d’être l’organisation de référence dans la promotion de l’entrepreneuriat jeune et de l’innovation au Burkina Faso.</p>
						</div>
					</div>
					<div class="sv-item item it2">
						<div class="hover-inner">
							<p>En vue d’atteindre nos objectifs, nous avons conçu l’initiative « YEER » (Young Entrepreneurship Empowerment Roadmap), une série d’activités méticuleusement mis en place de sorte à bénéficier à des “entrepreneurs naissant’’.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include_once("librairies/footer.php");?>
<?php include_once("librairies/scripts.php");?>
<script>
$(window).scrollPress();
</script>
</body>
</html>