<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7 no-js" lang="en-US">
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8 no-js" lang="en-US">
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html lang="en" class="no-js">
<head>
	<!-- Basic need -->
	<title>ETES-VOUS INTÉRESSÉS | Fondation Afrik Eveil</title>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="">
	<link rel="profile" href="#">
	<?php include_once("librairies/header.php");?>
</head>

<body>
<?php include_once("librairies/menu.php");?>
<div class="hero">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1>ETES-VOUS INTÉRESSÉS</h1>
				<img src="../commun/images/uploads/hero-line.png" alt="">
				<ul class="breadcumb">
					<li><a href="index.php">ACCUEIL</a></li>
					<li><span>/</span>ETES-VOUS INTÉRESSÉS</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="chooseus chooseusv2 ">
	<div class="container">
		<div class="row">
			<div class="heading-it">
				<h1>ETES-VOUS <span> INTÉRESSÉS?</span></h1>
				<img src="../commun/images/uploads/title-line.png" alt="">
			</div>
		</div>

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="chooseus-it">
					<img src="../commun/images/icon1.png" alt="">
					<div class="step-content">
						<h2>Devenez parrain</h2>
						<p>Vous êtes un expert, consultant expérimenté, coach ou homme d’affaires soucieux de l’emploi et l’entrepreneuriat des jeunes en Afrique.
						   Vous pouvez nous soutenir avec votre expertise et vos connaissances à travers des formations en gestion d’entreprises, en réseautage ou
						   dans l’accompagnement de nos membres et clients.
						</p>
					</div>
				</div>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="chooseus-it">
					<img src="../commun/images/icon2.png" alt="">
					<div class="step-content">
						<h2>Devenez membre</h2>
						<p>Etes-vous un(e) jeune africain(e) développant une idée, une initiative entrepreneuriale ou une solution spécifique au chômage des jeunes ?
						   Voulez-vous soutenir ou inspirer d’autres jeunes africains à œuvrer au développement de leurs pays ?
						   Si oui, veuillez remplir notre formulaire d’adhésion et nous envoyer votre CV et une brève description de votre projet. Nous offrons un
						   appui technique et logistique et un réseau  en vue d’aider à l’avancement de votre projet.
						</p>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="chooseus-it">
					<img src="../commun/images/icon1.png" alt="">
					<div class="step-content">
						<h2>Soyez l’un de nos sponsors</h2>
						<p>Nous offrons la possibilité aux organisations publiques, privées, régionales et internationales de soutenir nos projets et initiatives
						   par des primes, des donations et le renforcement des capacites.
						 </p>
					</div>
				</div>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="chooseus-it">
					<img src="../commun/images/icon6.png" alt="">
					<div class="step-content">
						<h2>Entrons en partenariat </h2>
						<p>Nous tenons a créer des partenariats avec les organisations de recherche, les cabinets de consulting, les structures de défense,
						   les fondations privées, les organisations regionales et internationales et les organisations de la société civile sur les projets qui vont dans la même perspective que nos objectifs.
						</p>
					</div>
				</div>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="chooseus-it">
					<img src="../commun/images/icon5.png" alt="">
					<div class="step-content">
						<h2>Clients Potentiels</h2>
						<p>Nous répondons à vos besoins de façon rapide et précise aux besoins des  entreprises, organisations de la société civile, organisation internationales travaillant dans le domaine de
						   l’entrepreneuriat et l’emploi des jeunes et recherchant des partenariats au niveau local.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!--div class="contactform consult">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-sm-12 col-xs-12">
				<div class="test-title">
					<h2>Contactez - Nous!</h2>
					<img  src="../commun/images/uploads/line-title.png" alt="">
					<p></p>
				</div>
			</div>
			<div class="col-md-8 col-sm-12 col-xs-12">
				<div class="left-it ct-form">
					<form class="ct-left">
						<div class="row">
							<div class="col-md-6 col-sm-12 col-xs-12">
								<h2>Je voudrais :</h2>
								<select>
									<option value="DevenirParrain">Devenir Parrain</option>
									<option value="DevenirMemmbre">Devenir Memmbre</option>
									<option value="DevenirPartenaire">Devenir Partenaire</option>
									<option value="DevenirClient">Devenir Client</option>
								</select>
							</div>
							<div class="col-md-6 col-sm-12 col-xs-12">
								<h2>Nom & Prénoms</h2>
								<input class="name" type="text" placeholder="Nom et prénoms">
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-sm-12 col-xs-12">
								<h2>Numéro De Téléphone(*)</h2>
								<input class="phone" type="text" placeholder="Votre numéro de téléphone">
							</div>
							<div class="col-md-6 col-sm-12 col-xs-12">
								<h2>Email</h2>
								<input class="phone" type="email" placeholder="Votre email">
							</div>
							<div class="col-md-6 col-sm-12 col-xs-12">
								<h2>Nous vous répondrons aussitôt que possible!</h2>
								<div>
									<input class="submit" type="submit" name="submit" value="Envoyer">
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div-->
<?php include_once("librairies/footer.php");?>
<?php include_once("librairies/scripts.php");?>
<script>
$(window).scrollPress();
</script>
</body>
</html>