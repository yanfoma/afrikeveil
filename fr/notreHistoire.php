<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7 no-js" lang="en-US">
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8 no-js" lang="en-US">
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html lang="en" class="no-js">
<head>
	<!-- Basic need -->
	<title>NOTRE HISTOIRE | Fondation Afrik Eveil</title>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="">
	<link rel="profile" href="#">
	<?php include_once("librairies/header.php");?>
</head>

<body>
<?php include_once("librairies/menu.php");?>
<div class="hero">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1>Notre Histoire</h1>
				<img src="../commun/images/uploads/hero-line.png" alt="">
				<ul class="breadcumb">
					<li><a href="index.php">ACCUEIL</a></li>
					<li><span>/</span>QUI NOUS SOMMES</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="testimonialv2 popular blog">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 popular-left">
				<div class="test-title left-it">
					<h2>NOTRE HISTOIRE</h2>
					<img  src="../commun/images/uploads/line-title.png" alt="">
					<p>La Foundation AFRIK EVEIL (A. E.) a été inspirée par Harambe Entrepreneur Alliance (HEA), un réseau d’étudiants et de jeunes professionnels d’universités et d’institutions renommées en Amerique du Nord, en Europe, en Asie et en Afrique travaillant à réaliser leurs aspirations entrepreneuriales. Pour plus d’informations sur HEA, voir <a href="http://www.healliance.org" target="_blank">www.healliance.org</a>
						<br>Quelques étapes importantes de notre parcours:</p>
				</div>
				<div class="row ">
					<div class="col-md-12">
						<div class="question-it">
							<div class="accordion-item">
								<button class="accordion"><h2>Juillet – septembre, 2010</h2></button>
								<div class="panel">
									<p> Consultations sur le terrain d’étudiants de différentes universités au Burkina Faso en acquérant un soutien communautaire et en établissant un réseau à la base.</p>
								</div>
							</div>
						</div>
						<div class="question-it">
							<div class="accordion-item">
								<button class="accordion"><h2>Février 2011 </h2></button>
								<div class="panel">
									<p> Formalisation de la Foundation AFRIK EVEIL (alors Harambe Burkina Faso Eveil) par l’adoption de ses statuts et règlement</p>
								</div>
							</div>
						</div>
						<div class="question-it">
							<div class="accordion-item">
								<button class="accordion"><h2>Avril 2011 </h2></button>
								<div class="panel">
									<p>AE est légalement reconnue au Burkina Faso.</p>
								</div>
							</div>
						</div>
						<div class="question-it">
							<div class="accordion-item">
								<button class="accordion"><h2>Septembre 2011</h2></button>
								<div class="panel">
									<p> AE organise  ses premiers ateliers au niveau nationale et forme des étudiants de differentes universités du Burkina Faso sur les compétences et outils requis réussir dans l’entrepreneuriat.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include_once("librairies/footer.php");?>
<?php include_once("librairies/scripts.php");?>
<script>
$(window).scrollPress();
</script>
</body>
</html>