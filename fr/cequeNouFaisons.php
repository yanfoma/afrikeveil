<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7 no-js" lang="en-US">
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8 no-js" lang="en-US">
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html lang="en" class="no-js">
<head>
	<!-- Basic need -->
	<title>CE QUE NOUS FAISONS | Fondation Afrik Eveil</title>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="">
	<link rel="profile" href="#">
	<?php include_once("librairies/header.php");?>
		<style type="text/css">
		.services .item h2{

			font-size:20px;
		}

		.services .item p{

			text-align: justify;
		}

	</style>
</head>

<body>
<?php include_once("librairies/menu.php");?>
<div class="hero">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1>CE QUE NOUS FAISONS</h1>
				<img src="../commun/images/uploads/hero-line.png" alt="">
				<ul class="breadcumb">
					<li><a href="index.php">ACCUEIL</a></li>
					<li><span>/</span>CE QUE NOUS FAISONS</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="chooseus">
	<div class="container">
		<div class="row">
			<div class="heading-it">
				<h1>Nos valeurs <span><br><br> Qualité - Intégrité - Utilité sociale</span></h1>
				<img src="../commun/images/uploads/title-line.png" alt="">
				<p style="font-size:19px;">
					<br><br>Nous dotons les start-up, les politiques publiques et la société civile de solutions créatives sur les défis entrepreneuriaux à travers:
				</p>
			</div>
		</div>
	</div>
</div>
<div class="services">
	<div class="container">
		<div class="row">
			<div id="service-testimonial">
				<div class="sv-item item">
					<h2>La Recherche</h2>
					<p>Notre équipe d’experts examine les problèmes posés et propose des analyses indépendantes et des recommendations pertinentes sur les sujets en
					   lien avec les politiques publiques, l’entrepreneuriat et le développement des affaires.
					   Les résultats de nos recherches constituent la base de nos services de conseil et nos experts sont soigneusement choisis en fonction de leurs
					   domaines de compétence afin d’optimiser leurs aptitudes et de traiter les projets en toute confiance.
					   Nous sommes convaincus que mettre les informations crédibles et utiles à la disposition des personnes indiquées au bon moment contribute a la
					   veille citoyenne, influence les alternatives politiques et améliore les décisions d’affaires.
					</p>
				</div>
				<div class=" sv-item item ">
					<h2>Le Conseil</h2>
					<p>Focalisés sur les solutions pratiques, nous offrons un conseil stratégique aux structures publiques, aux organisations internationales et
					   aux organisations de la société civile en matière d’entrepreneuriat et de lutte contre le chômage.
					   Dans un contexte marqué par le flot d’information et d’opinions conflictuelles, nous facilitons la prise de décision à travers une analyse
					   rigoureuse coût-bénéfices pour offrir des solutions pratiques et durables aux décideurs
					   Nous offrons des conseils en stratégies de communication.
					</p>
				</div>
				<div class=" sv-item item ">
					<h2>L’Incubation</h2>
					<p>Nous soutenons le développement des entreprises et des start-up en Afrique à travers nos formation en management, le coaching, l’accompagnement,
					   le réseautage et la stratégie numérique.
					   Nous sommes motivés par la ferme conviction que les idées innovatrices, appliquées avec le sens des affaires, sont essentielles dans la lutte contre
					   la pauvreté et la réduction du chômage des jeunes en Afrique.
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include_once("librairies/footer.php");?>
<?php include_once("librairies/scripts.php");?>
<script>
$(window).scrollPress();
</script>
</body>
</html>