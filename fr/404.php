<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7 no-js" lang="en-US">
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8 no-js" lang="en-US">
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html lang="en" class="no-js">
<head>
	<!-- Basic need -->
	<title>404 | Fondation Afrik Eveil</title>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="">
	<link rel="profile" href="#">
	<?php include_once("librairies/header.php");?>
</head>

<body>
<?php include_once("librairies/menu.php");?>
<div class="hero">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1>404</h1>
				<img src="../commun/images/uploads/hero-line.png" alt="">
				<ul class="breadcumb">
					<li><a href="index.php">ACCUEIL</a></li>
					<li><span>/</span>404</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="error">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<img src="../commun/images/uploads/error-img.png" alt="">
				<img src="../commun/images/uploads/error-line.png" alt="">
				<h1>La page que vous recherchez est pas introuvable</h1>
				<p>La page que vous recherchez n'existe pas. Elle a peut-être été déplacée ou retirée tout simplement. Veuillez revenir à la<br>page d'accueil et voir si vous trouverez ce que vous recherchez.</p>
				<a href="index.php">ACCUEIL</a>
			</div>
		</div>
	</div>
</div>
<?php include_once("librairies/footer.php");?>
<?php include_once("librairies/scripts.php");?>
<script>
$(window).scrollPress();
</script>
</body>
</html>