<?php

	include("../commun/config.php");
	include("../commun/db.php");
	include("../commun/function.php");
	$query = "SELECT * FROM posts WHERE language='fr' ORDER BY id DESC";
	$posts = $db->query($query);
?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7 no-js" lang="en-US">
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8 no-js" lang="en-US">
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html lang="en" class="no-js">
<head>
	<!-- Basic need -->
	<title>NOS ACTUALITES | Fondation Afrik Eveil</title>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="">
	<link rel="profile" href="#">
	<?php include_once("librairies/header.php");?>
</head>

<body>
	<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.10&appId=534584460076741";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<?php include_once("librairies/menu.php");?>
<div class="hero">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1>NOS ACTUALITES</h1>
				<img src="../commun/images/uploads/hero-line.png" alt="">
				<ul class="breadcumb">
					<li><a href="index.php">ACCUEIL</a></li>
					<li><span>/</span>NOS ACTUALITES</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="bloglistpost-v1 blogv3 blog servicesingle">
	<div class="container">
		<div class="row">
			<div class="col-md-9 col-sm-12 col-xs-12">
				<div class="blogpost-v1">
					<?php if($posts->num_rows > 0){
						while($row = $posts->fetch_assoc()){
					?>
						<div class="blog-it normal2">
						<div class="post-thumbnail">
							<a href="#"><img class="post-img" src="../admin/uploads/<?php echo $row['image'];?>" alt="thumb1"></a>
						</div>
						<div class="blog-it-left">
							<div class="row">
								<div class="col-md-2 col-sm-3 col-xs-12">
									<div class="date">
										<h1><?php
												$date=$row['date'];
												getMonthDay($date);?></h1>
										<img src="../commun/images/uploads/white-line.png" alt="">
										<p><?php
												$date=$row['date'];
												getMonth($date);?></p>
									</div>
								</div>
								<div class="col-md-10 col-sm-9 col-xs-12">
									<div class="inner-ct">
										<h2><a href="single.php?post=<?php echo $row['id']; ?>"><?php echo $row['title'];?></a></h2>
										<div class="date-inner">
											<span><i class="ion-android-person"></i><em>Par</em> <?php echo $row['author'];?></span>
											<span>|</span>
											<span><i class="fa fa-calendar" aria-hidden="true"></i><em>
												<?php
													getMonthDay($date);echo " ";getMonth($date);echo " ";getMonthYear($date);
												?>
											</span>
										</div>
										<img class="divide2" src="../commun/images/uploads/blogline.png" alt="">
										<p style="text-align:justify;"><?php
												$body = $row['body'];
												echo substr($body, 0, 400)."...";?></p>
										<a class="read" href="single.php?post=<?php echo $row['id']; ?>">Lire Plus</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php }}?>
					<!--ul class="pagination">
						<p>page: </p>
						<li class="num active"><a href="#">01</a></li>
						<li class="num"><a href="#">02</a></li>
						<li class="num2"><a href="#">...</a></li>
						<li class="num"><a href="#">30</a></li>
						<li class="icon"><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
					</ul-->
				</div>
			</div>
			<?php include_once("librairies/sidebar.php");?>
		</div>
	</div>
</div>
<?php include_once("librairies/footer.php");?>
<?php include_once("librairies/scripts.php");?></body>
<script>
$(window).scrollPress();
</script>
</html>