<?php
/**
 * This example shows how to handle a simple contact form.
 */

$msg = '';
//Don't run this unless we're handling a form submission
//if (array_key_exists('email', $_POST)) {
if (isset($_POST['submit'])) {
    date_default_timezone_set('Etc/UTC');

    require '../commun/PHPMailer/PHPMailerAutoload.php';

    //Create a new PHPMailer instance
    $mail = new PHPMailer;
    //Tell PHPMailer to use SMTP - requires a local mail server
    //Faster and safer than using mail()
    $mail->isSMTP();
    //Enable SMTP debugging
    // 0 = off (for production use)
    // 1 = client messages
    // 2 = client and server messages
    $mail->SMTPDebug = 2;

    //Ask for HTML-friendly debug output
    $mail->Debugoutput = 'html';

    //Set the hostname of the mail server
    //$mail->Host = 'smtp.gmail.com';
    // use
    $mail->Host = gethostbyname('smtp.gmail.com');
    // if your network does not support SMTP over IPv6

    //Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
    $mail->Port = 587;

    //Set the encryption system to use - ssl (deprecated) or tls
    $mail->SMTPSecure = 'tls';

    //Whether to use SMTP authentication
    $mail->SMTPAuth = true;

    //Username to use for SMTP authentication - use full email address for gmail
    $mail->Username = "fabioued10@gmail.com";

    //Password to use for SMTP authentication
    $mail->Password = "ThankGod@1993";
    //Use a fixed address in your own domain as the from address
    //**DO NOT** use the submitter's address here as it will be forgery
    //and will cause your messages to fail SPF checks
    $mail->setFrom('afrikevil@yahoo.fr', 'First Last');
    //Send the message to yourself, or whoever should receive contact for submissions
    $mail->addAddress('fabioued@yahoo.fr', 'Fabrice Ouedraogo');
    //Put the submitter's address in a reply-to header
    //This will fail if the address provided is invalid,
    //in which case we should ignore the whole request
    if ($mail->addReplyTo('fabioued@yahoo.fr', $_POST['name'])) {
        $mail->Subject = $_POST['subject'];
        //Keep it simple - don't use HTML
        $mail->isHTML(false);
        //Build a simple message body
        $mail->Body = <<<EOT
		Email: {$_POST['email']}
		Name: {$_POST['name']}
		Message: {$_POST['message']}
EOT;
        //Send the message, check for errors
        if (!$mail->send()) {
            //The reason for failing to send will be in $mail->ErrorInfo
            //but you shouldn't display errors to users - process the error, log it on your server.
            $msg = 'Sorry, something went wrong. Please try again later.';
        } else {
            $msg = 'Message sent! Thanks for contacting us.';
        }
    } else {
        $msg = $mail->ErrorInfo.'Invalid email address, message ignored.';
    }
}
//echo $_POST['email'];
?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7 no-js" lang="en-US">
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8 no-js" lang="en-US">
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html lang="en" class="no-js">
<head>
	<!-- Basic need -->
	<title>Contact | Fondation Afrik Eveil</title>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="">
	<link rel="profile" href="#">
	<?php include_once("librairies/header.php");?>
	<style>
#map {
  height: 400px;
  margin-bottom: 20px;
}

.active {
  max-width: 150px;
  background: #099;
  color: #fff;
}
</style>
</head>

<body>
<?php include_once("librairies/menu.php");?>
<div class="hero">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1>Contact </h1>
				<img src="../commun/images/uploads/hero-line.png" alt="">
				<ul class="breadcumb">
					<li><a href="index.php">ACCEUIL</a></li>
					<li><span>/</span> Contact</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="contactpage servicesingle map-contact" id="map-check">
	<div class="container">
		<div class="row">
			<div class="contact-it sidebar">
				<div class="col-md-4 col-sm-12 col-xs-12">
					<div class="sb-it">
						<div class="sb-title">
							<h2>Nos Contacts</h2>
						</div>
						<div class="sb-content">
							<div class="ct-it">
								<i class="fa fa-map-marker" aria-hidden="true"></i>
								<p>Cité Azimmo Ouaga 2000, 111 BP 1597 Ouagadougou CMS 11—Burkina Faso</p>
							</div>
							<div class="ct-it">
								<i class="ion-android-phone-portrait"></i>
								<p>  +226 70707070</p>
							</div>
							<div class="ct-it">
								<i class="ion-email"></i>
								<p><a href="#">info@afrikeveil.org</a><br/>
									<a href="#">Ghana:ekumi@afrikeveil.org</a>
								</p>
							</div>
							<div class="ct-icon">
								<a href="https://www.facebook.com/afrikeveil.org/" target="_blank" class="hvr-grow"><i class="ion-social-facebook "></i></a>
								<a href="https://twitter.com/a_eveil" 			   target="_blank" class="hvr-grow"><i class="ion-social-twitter"></i></a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-8 col-sm-12 col-xs-12">
					<!--div id="map_canvas" style="width:100%;height:430px">
					</div-->
				  <div id="map"></div>
					<div id="dragend"></div>
					<div id="bounds_changed"></div>
				  	<script src='https://maps.googleapis.com/maps/api/js'></script>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="contactv1">
	<div class="container contact-ct">
		<div class="row">
			<div class="col-md-9 col-sm-8 col-xs-12">
				<h1>Envoyer Nous Un message sur Messenger</h1>
			</div>
			<div class="col-md-3 col-sm-4 col-xs-12">
				<div class="contact-bt">
					<a href="https://m.me/afrikeveil.org" target="_blank" class="readmore2">Envoyer</a>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include_once("librairies/footer.php");?>
<?php include_once("librairies/scripts.php");?>
<script>
$(window).scrollPress();
</script>
</body>
</html>