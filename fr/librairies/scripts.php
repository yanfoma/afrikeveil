<script src="https://code.jquery.com/jquery-3.1.1.js"></script>
<script src="../commun/js/plugins.js"></script>
<script src="../commun/js/plugins2.js"></script>
<script src="../commun/js/custom.js"></script>
<!--scrollPress JS BACK TO TOP-->
<script src="../commun/js/scrollPress.js"></script>
<script async defer src="../commun/js/maps.js"type="text/javascript"></script>
<script src="../commun/js/jquery.polyglot.language.switcher.js" type="text/javascript"></script>
 <script type="text/javascript">
        $(document).ready(function() {
            $('#polyglotLanguageSwitcher').polyglotLanguageSwitcher({
				effect: 'fade',
                testMode: true,
               onChange: function(evt) {
            	var url= window.location.href;
            	var lang= evt.selectedItem;
                var search= window.location.search;
            	switch(lang){
            		case "fr":
            			url = url.replace('/en', '/fr');
                        url += search;
            	 		window.location.href = url;
            	 	break;
            	 	case "en":
            			url = url.replace('/fr', '/en');
                        url += search;
            	 		window.location.href = url;
            	 	break;
            		}
            	}
			});
        });
    </script>

    <!-- Global Site Tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-51805819-5"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments)};
  gtag('js', new Date());

  gtag('config', 'UA-51805819-5');
</script>
