<?php
$tw_username = 'A_eveil';
$data = file_get_contents('https://cdn.syndication.twimg.com/widgets/followbutton/info.json?screen_names='.$tw_username);
$parsed =  json_decode($data,true);
$tw_followers =  $parsed[0]['followers_count'];
$tw_name =  $parsed[0]['name'];
?>
<div class="col-md-3 col-sm-12 col-xs-12">
	<div class="bg-sidebar sidebar ">
		<div class="sb-it">
			<div class="sb-title">
				<h2><?php echo "$tw_name";
						  echo "<br>";
						  echo "Abonnés: $tw_followers";?></h2>
				<img src="../commun/images/uploads/line-title2.png" alt="">
			</div>
			<div class="sb-content">
				<a class="twitter-timeline" href="https://twitter.com/A_eveil" data-width="300"
  					data-height="1500"></a>
  				<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
  				<a class="twitter-follow-button"href="https://twitter.com/A_eveil" data-size="large">Follow @A_eveil</a>
			</div>
		</div>
		<div class="pp-posts sb-it">
			<div class="sb-title">
				<h2>Nous Suivre Sur Facebook</h2>
				<img src="../commun/images/uploads/line-title2.png" alt="">
			</div>
			<div class="pp-post-it">
				<div class="row">
					<div class="fb-page" data-href="https://www.facebook.com/afrikeveil.org/" data-tabs="timeline" data-width="300" data-height="1500" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/afrikeveil.org/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/afrikeveil.org/">AFRIK EVEIL</a></blockquote></div>
					<br>
					<br>
					<div class="fb-like" data-href="https://www.facebook.com/afrikeveil.org/" data-width="300" data-layout="standard" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
				</div>
			</div>
		</div>
	</div>
</div>