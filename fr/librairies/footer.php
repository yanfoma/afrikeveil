<footer class="ht-footer">
	<div class="container">
		<div class="row">
			<div class="col-md-3 col-sm-12 col-xs-12">
				<div class="ft-contact">
					<div class="ft-heading">
						<h6>À propos</h6>
					</div>
					<div class="ct-it">
						<p  style="text-align:left; color: #888888;">Afrik Eveil est une organisation à but non lucratif, créée et gérée par des professionnels expérimentés qui se moblisent en faveur de l'entrepreneuriat comme moyen efficace de promotion de l’employabilité des jeunes en Afrique.
						</p>
					</div>
				</div>
			</div>
			<div class="col-md-6 col-sm-12 col-xs-12">
				<div class="ft-infor">
					<div class="ft-heading">
						<h6>Afrik Eveil Sitemap</h6>
					</div>
					<div class="infor-it">
						<ul class="left">
							<li><i class="fa fa-angle-right" aria-hidden="true"></i><a href="index.php">Acceuil</a></li>
							<li><i class="fa fa-angle-right" aria-hidden="true"></i><a href="notreOrganisation.php">Notre Organisation</a></li>
							<li><i class="fa fa-angle-right" aria-hidden="true"></i><a href="notreHistoire.php">Notre Histoire</a></li>
							<li><i class="fa fa-angle-right" aria-hidden="true"></i><a href="notreEquipe.php">Notre Equipe</a></li>
							<li><i class="fa fa-angle-right" aria-hidden="true"></i><a href="cequeNouFaisons.php">Ce Que NousFaisons</a></li>
							<li><i class="fa fa-angle-right" aria-hidden="true"></i><a href="estVousInterrese.php">Etes-Vous Interessé?</a></li>
						</ul>
						<ul class="left">
							<li><i class="fa fa-angle-right" aria-hidden="true"></i><a href="nosactualites.php">Nos Actualités</a></li>
							<li><i class="fa fa-angle-right" aria-hidden="true"></i><a href="blog.php">Blog</a></li>
							<li><i class="fa fa-angle-right" aria-hidden="true"></i><a href="contact.php">Contact</a></li>
							<li><i class="fa fa-angle-right" aria-hidden="true"></i><a href="ressources.php">Ressources</a></li>
							<li><i class="fa fa-angle-right" aria-hidden="true"></i><a href="discours.php">Discours du Président à la chambre des Lords</a></li>

						</ul>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-12 col-xs-12">
				<div class="ft-contact">
					<div class="ft-heading">
						<h6>Nos Contacts</h6>
					</div>
					<ul class="ct-it">
						<li><i class="fa fa-map-marker" aria-hidden="true"></i><a href="#">Cité Azimmo Ouaga 2000, 111 BP 1597 Ouagadougou CMS 11—Burkina Faso</a></li><br/>
						<li><i class="fa fa-envelope" aria-hidden="true"></i><a href="#">Email:  info@afrikeveil.org</a></li><br/>
						<li><i class="ion-ios-telephone"></i><a href="#">Portable: +226 25 66 64 60</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</footer>
<div class="ft-below">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="ft-below-left">
					<center><p>Fondation Afrik Eveil  © 2017 Tous Droits Réservés. Conception <a class="readmore2"href="https://yanfoma.tech" target="_blank">Yanfoma</a></center>
				</div>
			</div>
		</div>
	</div>
</div>