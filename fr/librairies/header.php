
	<link rel="shortcut icon" href="../../commun/images/favicon.ico">
    <link rel="apple-touch-icon" sizes="180x180" href="../../commun/images/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../../commun/images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../../commun/images/favicon-16x16.png">
    <link rel="mask-icon" href="../../commun/images/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="theme-color" content="#ffffff">
    <meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="">
	<link rel="profile" href="#">
	<!-- <link rel="shortcut icon" href="images/favicon.ico"> -->

	<!-- Mobile specific meta -->

	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
	<meta name="format-detection" content="telephone-no">
	<!-- Google Fonts-->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Poppins:400,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,700,300i" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
	<!-- Latest compiled and minified CSS -->
	<!-- CSS files -->
	<link rel="stylesheet" href="../commun/css/plugins.css">
	<link rel="stylesheet" href="../commun/css/style.css">
	<!--polyglot CSS -->
    <link href="../commun/css/polyglot-language-switcher.css" type="text/css" rel="stylesheet">
	<!--scrollPress CSS -->
	<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link href="../commun/css/scrollPress.css" rel="stylesheet" type="text/css">
    <!--Marquee CSS -->
    <link href="../commun/css/marquee.css" rel="stylesheet" type="text/css">