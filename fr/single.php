<?php
	include("../commun/config.php");
	include("../commun/db.php");
	include("../commun/function.php");

	if (isset($_GET['post'])){
		$id = mysqli_real_escape_string($db, $_GET['post']);
		$query = "SELECT * FROM posts WHERE id='$id'";
	//si l'utilisateur rentre autre q'un nombre,on renvoie a la page d'actualites
		if(!is_numeric($id)){
			header("location:nosactualites.php");
			exit();
		}
	}
	$posts = $db->query($query);
	$row = $posts->fetch_assoc();
	$total_posts = $db->query("SELECT * FROM posts");
	$count = $total_posts->num_rows;
	//si l'utilisateur rentre un nombre plus eleve que le nombre de posts on renvoie a la page d'actualites
	if($id > $count){
		header("location:nosactualites.php");
		exit();
	}


	function time_diff_string($from, $to, $full = false) {
    $from   = new DateTime($from);
    $to     = new DateTime($to);
    $diff   = $to->diff($from);
    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;
    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . '' : 'à l/\'\instant';
}


function currentPageURL() {
    $curpageURL = 'http';
    if ($_SERVER["HTTPS"] == "on") {
        $curpageURL.= "s";
    }
    $curpageURL.= "://";
    if ($_SERVER["SERVER_PORT"] != "80") {
        $curpageURL.= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
    }
    else {
        $curpageURL.= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
    }
    return $curpageURL;
}

$image_og = "https://afrikeveil.org/admin/uploads/$row[image]";
$desc = $row['title'];
$tweet = substr($desc, 0,70);
?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7 no-js" lang="en-US">
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8 no-js" lang="en-US">
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html lang="en" class="no-js">
<head>
	<!-- Basic need -->
	<title><?php echo $row['title'];?>| Fondation Afrik Eveil</title>
	<meta charset="UTF-8" />
    <meta property="og:site_name"       content="www.afrikeveil.org" />
    <meta property="og:title"           content="<?php echo $row['title'];?>" />
    <meta property="og:type"            content="website" />
    <meta property="og:url"             content="https://afrikeveil.org" />
    <meta property="og:description"     content="<?php echo $desc;?>" />
    <meta property="og:updated_time"    content="<?php echo $row['date'];?>">
    <meta property="og:image"           content="<?php echo $image_og;?>" />
    <meta property="og:locale"          content="fr" />
    <?php include_once("librairies/header.php");?>
</head>


<body>
	<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId            : '400025927061215',
      autoLogAppEvents : true,
      xfbml            : true,
      version          : 'v2.10'
    });
    FB.AppEvents.logPageView();
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.10&appId=534584460076741";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div id="fb-root"></div>
  <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>

<?php include_once("librairies/menu.php");?>
<div class="hero">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1> <?php echo $row['title'];?></h1>
				<img src="../commun/images/uploads/hero-line.png" alt="">
				<ul class="breadcumb">
					<li><a href="index.php">ACCEUIL</a></li>
					<li><span>/</span> NOS ACTUALITES</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="bloglistpost-v1 blogv3 blog servicesingle blogsingle">
	<div class="container">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="blogpost-v1">
					<div class="blog-it slider blogsingle-ct">
						<div class="post-thumbnail">
							<div id="owl-thumbnail">
								<div class="item">
									<img class="post-img" src="../admin/uploads/<?php echo $row['image'];?>" alt="thumb1">
								</div>
							</div>
						</div>
						<div class="blog-it-left">
							<div class="row">
								<div class="col-md-2 col-sm-12 col-xs-12">
									<div class="date">
										<h1><?php
												$date=$row['date'];
												getMonthDay($date);?></h1>
										<img src="../commun/images/uploads/white-line.png" alt="">
										<p><?php
												$date=$row['date'];
												getMonth($date);?></p>
									</div>
								</div>
								<div class="col-md-10 col-sm-12 col-xs-12">
									<div class="inner-ct">
										<h2><a href="#"><?php echo $row['title'];?></a></h2>
										<div class="date-inner">
											<span><i class="ion-eye-person"></i><em>Par</em> <?php echo $row['author'];?></span>
											<span>|</span>
											<span><i class="fa fa-calendar" aria-hidden="true"></i><em><?php getMonthDay($date);echo " ";getMonth($date);echo " ";getMonthYear($date);?></em></span>
										</div>
										<img class="divide2" src="../commun/images/uploads/blogline.png" alt="">
										<p style="text-align:justify;"><?php echo $row['body'];?></p>
									</div>
									<!-- tags -->
									<div class="share">
										<div class="row">
											<div class="col-md-3">
											</div>
											<div class="col-md-9">
												<?php
													$title = $row['body'];
													$tweet = substr($title, 0,70);
                                                    $title=urlencode($$title);
                                                    $url= urlencode('$currentPageURL()');
                                                    $summary=urlencode("Content you can dynamically display like: (E.g.: Time:".(($usertime < 3600)?($usertime.' Seconds'): (round($usertime/60, 2))." Minutes, Points".$userscore.") ") );
                                                    $images = "https://afrikeveil.org/admin/uploads/$row[image]";
                                                    $image=urlencode('$images');
												?>
												<div class="social-icon">
                                                    <p style="margin-right: 20px; color:black; font-weight:bold">Partager</p>
                                                        <a onClick="window.open('http://www.facebook.com/sharer.php?s=100&amp;p[title]=<?php echo $title;?>&amp;p[summary]=<?php echo $summary;?>&amp;p[url]=<?php echo $url; ?>&amp;p[images][0]=<?php echo $image;?>','sharer','toolbar=0,status=0,width=550,height=300');" href="javascript: void(0)">share</a>
                                                        <a href="javascript:fbshareCurrentPage()" target="_blank" alt="Share on Facebook"><i class="ion-social-facebook"></i></a>
														<!--a class="w-inline-block social-share-btn fb" 	 alt="Share on Facebook" 	href="https://www.facebook.com/sharer/sharer.php?u=" title="Afrik Eveil"><i class="ion-social-facebook"></i></a-->
                                                        <a href="https://twitter.com/share?url=https://afrikeveil.org&amp;text=afrikeveilPost&amp;hashtags=afrikeveil" target="_blank"><i class="ion-social-twitter"></i></a>
                                                        <!--a class="w-inline-block social-share-btn gplus" alt="Share on Google Plus" href="https://plus.google.com/share?url=" 				 						   title="Share on Google+" 	onclick="window.open('https://plus.google.com/share?url=' + encodeURIComponent(document.URL)); return false;"><i class="ion-social-googleplus"></i></a-->
														<!--a class="w-inline-block social-share-btn lnk" 	 alt="Share on LinkedIn" 	href="http://www.linkedin.com/shareArticle?mini=true&url=&title=&summary=&source=" title="Share on LinkedIn" 	onclick="window.open('http://www.linkedin.com/shareArticle?mini=true&url=' + encodeURIComponent(document.URL) + '&title=' + encodeURIComponent(document.title)); return false;"><i class="ion-social-linkedin"></i></a-->
														<a class="w-inline-block social-share-btn email" alt="Share via Mail" 		href="mailto:?subject=&body=:%20"  												   title="Afrik Eveil" 			onclick="window.open('mailto:?subject=' + encodeURIComponent(document.title) + '&body=' + encodeURIComponent(document.URL)); return false;"><i class="fa fa-envelope-o"></i></a>
												</div>
											</div>
									</div>
									<img class="div-line" src="../commun/images/uploads/div-line.png" alt="divide">
									<!-- comment -->
									<div class="comment">
										<div id="disqus_thread"></div>
											<div id="disqus_thread"></div>
												<script>
													(function() { // DON'T EDIT BELOW THIS LINE
													var d = document, s = d.createElement('script');
													s.src = 'https://http-188-226-149-64-afrikeveil.disqus.com/embed.js';
													s.setAttribute('data-timestamp', +new Date());
													(d.head || d.body).appendChild(s);
													})();
												</script>
												<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include_once("librairies/footer.php");?>
<?php include_once("librairies/scripts.php");?>
<script>
$(window).scrollPress();
</script>
    <script language="javascript">
        function fbshareCurrentPage()
        {window.open("https://www.facebook.com/sharer/sharer.php?u="+escape(window.location.href)+"&t="+document.title, '',
            'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');
            return false; }
    </script>
<script id="dsq-count-scr" src="//http-188-226-149-64-afrikeveil.disqus.com/count.js" async></script>
</body>
</html>