<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7 no-js" lang="en-US">
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8 no-js" lang="en-US">
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html lang="en" class="no-js">
<head>
	<!-- Basic need -->
	<title>OUR HISTORY | Afrik Eveil Foundation</title>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="">
	<link rel="profile" href="#">
	<?php include_once("librairies/header.php");?>
</head>

<body>
<?php include_once("librairies/menu.php");?>
<div class="hero">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1>Our History</h1>
				<img src="../commun/images/uploads/hero-line.png" alt="">
				<ul class="breadcumb">
					<li><a href="index.php">HOME</a></li>
					<li><span>/</span>OUR HISTORY</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="testimonialv2 popular blog">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 popular-left">
				<div class="test-title left-it">
					<h2>OUR HISTORY</h2>
					<img  src="../commun/images/uploads/line-title.png" alt="">
					<p>The AFRIK EVEIL (A.E.) Foundation was inspired by and is part of  Harambe Entrepreneur Alliance (HEA), an alliance of students and young African professionals from reputable universities and institutions in North America, Europe, Asia, and Africa working to realize their entrepreneurial aspirations. More  information on HEA can be found at <a href="http://www.healliance.org" target="_blank">www.healliance.org</a>
				</div>
				<div class="row ">
					<div class="col-md-12">
						<div class="question-it">
							<div class="accordion-item">
								<button class="accordion"><h2>July – Sept, 2010</h2></button>
								<div class="panel">
									<p> On-the-ground consultation of students from different universities in Burkina Faso in rallying  community support and establishing grass root network.</p>
								</div>
							</div>
						</div>
						<div class="question-it">
							<div class="accordion-item">
								<button class="accordion"><h2>February 2011 </h2></button>
								<div class="panel">
									<p> Formalization of the  A.E. Foundation (then Harambe Burkina Faso Eveil) by voting on, adopting and implementing its constitution and bylaws.</p>
								</div>
							</div>
						</div>
						<div class="question-it">
							<div class="accordion-item">
								<button class="accordion"><h2>April 2011 </h2></button>
								<div class="panel">
									<p> HBFE was legally recognized in Burkina Faso.</p>
								</div>
							</div>
						</div>
						<div class="question-it">
							<div class="accordion-item">
								<button class="accordion"><h2>September 2011</h2></button>
								<div class="panel">
									<p> HBFE held its first National Convention in Ouagadougou, Burkina Faso.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include_once("librairies/footer.php");?>
<?php include_once("librairies/scripts.php");?>
<script>
$(window).scrollPress();
</script>
</body>
</html>