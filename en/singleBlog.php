<?php
	include("../commun/config.php");
	include("../commun/db.php");
	include("../commun/function.php");
	if (isset($_GET['post'])){
		$id = mysqli_real_escape_string($db, $_GET['post']);
		$query = "SELECT * FROM blog WHERE id='$id'";
	//si l'utilisateur rentre autre q'un nombre,on renvoie a la page d'actualites
		if(!is_numeric($id)){
			header("location:blog.php");
			exit();
		}
	}
	$posts = $db->query($query);
	$row = $posts->fetch_assoc();
	$total_posts = $db->query("SELECT * FROM blog");
	$count = $total_posts->num_rows;
	//si l'utilisateur rentre un nombre plus eleve que le nombre de posts on renvoie a la page d'actualites
	if($id > $count){
		header("location:blog.php");
		exit();
	}

	if(isset($_POST['post_comment'])){
		$name    = mysqli_real_escape_string($db,$_POST['name']);
		$comment = mysqli_real_escape_string($db,$_POST['comment']);
			if(isset($_POST['email']))
			{
				$email = mysqli_real_escape_string($db,$_POST['email']);
			}
			else{
				$email 	 = "";
			}
		$query = "INSERT INTO commentsBlog (post,name,email,comment,dateComment) VALUES ('$id', '$name', '$email', '$comment',CURRENT_TIMESTAMP())";
			if($db->query($query)){
				header("location: single.php?post=$id");
				exit();
			} else {
			  echo "<script>alert('An error has occurred. Please try again');</script>";

			}
	}
	$query= "SELECT * FROM commentsBlog WHERE post='$id'";
	$comment = $db->query($query);


	function time_diff_string($from, $to, $full = false) {
    $from   = new DateTime($from);
    $to     = new DateTime($to);
    $diff   = $to->diff($from);
    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;
    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . '' : 'à l/\'\instant';
}

?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7 no-js" lang="en-US">
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8 no-js" lang="en-US">
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html lang="en" class="no-js">
<head>
	<!-- Basic need -->
	<title><?php echo $row['title'];?>| Afrik Eveil Foundation</title>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="">
	<link rel="profile" href="#">
	<?php include_once("librairies/header.php");?>
</head>

<body>
<?php include_once("librairies/menu.php");?>
<div class="hero">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1> <?php echo $row['title'];?></h1>
				<img src="../commun/images/uploads/hero-line.png" alt="">
				<ul class="breadcumb">
					<li><a href="index.php">HOME</a></li>
					<li><span>/</span> BLOG</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="bloglistpost-v1 blogv3 blog servicesingle blogsingle">
	<div class="container">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="blogpost-v1">
					<div class="blog-it slider blogsingle-ct">
						<div class="post-thumbnail">
							<div id="owl-thumbnail">
								<div class="item">
									<img class="post-img" src="../admin/uploads/<?php echo $row['image'];?>" alt="thumb1">
								</div>
							</div>
						</div>
						<div class="blog-it-left">
							<div class="row">
								<div class="col-md-2 col-sm-12 col-xs-12">
									<div class="date">
										<h1><?php
												$date=$row['date'];
												getMonthDay($date);?></h1>
										<img src="../commun/images/uploads/white-line.png" alt="">
										<p><?php
												$date=$row['date'];
												getMonthEn($date);?></p>
									</div>
								</div>
								<div class="col-md-10 col-sm-12 col-xs-12">
									<div class="inner-ct">
										<h2><a href="#"><?php echo $row['title'];?></a>

											<script type="text/javascript">
												var url = window.location.pathname;
												var search= window.location.search;
												url += search;
												//alert(url);
											</script>
										</h2>
										<div class="date-inner">
											<span><i class="ion-eye-person"></i><em>By</em> <?php echo $row['author'];?></span>
											<span>|</span>
											<span><i class="fa fa-calendar" aria-hidden="true"></i><em><?php echo $row['date'];?></em></span>
										</div>
										<img class="divide2" src="../commun/images/uploads/blogline.png" alt="">
										<p style="text-align:justify;"><?php echo $row['body'];?></p>
									</div>
									<!-- tags -->
									<div class="share">
										<div class="row">
											<div class="col-md-6">
												<p>More News &nbsp;&nbsp;</p>
											</div>
											<div class="col-md-6">
												<div class="social-icon">
													<p style="margin-right: 20px; color:black; font-weight:bold">Share </p>
														<a class="w-inline-block social-share-btn fb" 	 alt="Share on Facebook" 	href="https://www.facebook.com/sharer/sharer.php?u=&t=" 						   title="Afrik Eveil" 			onclick="window.open('https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(document.URL) + '&t=' + encodeURIComponent(document.URL)); return false;"><i class="ion-social-facebook"></i></a>
														<a class="w-inline-block social-share-btn tw" 	 alt="Share on Twitter" 	href="https://twitter.com/intent/tweet?"                 						   title="Afrik Eveil" 			onclick="window.open('https://twitter.com/intent/tweet?text=<?php echo "$tweet...";?>' + ':%20 ' + encodeURIComponent(document.URL)); return false;" data-hashtags="AfrikEveil"><i class="ion-social-twitter"></i></a>
														<a class="w-inline-block social-share-btn gplus" alt="Share on Google Plus" href="https://plus.google.com/share?url=" 				 						   title="Share on Google+" 	onclick="window.open('https://plus.google.com/share?url=' + encodeURIComponent(document.URL)); return false;"><i class="ion-social-googleplus"></i></a>
														<a class="w-inline-block social-share-btn lnk" 	 alt="Share on LinkedIn" 	href="http://www.linkedin.com/shareArticle?mini=true&url=&title=&summary=&source=" title="Share on LinkedIn" 	onclick="window.open('http://www.linkedin.com/shareArticle?mini=true&url=' + encodeURIComponent(document.URL) + '&title=' + encodeURIComponent(document.title)); return false;"><i class="ion-social-linkedin"></i></a>
														<a class="w-inline-block social-share-btn email" alt="Share via Mail" 		href="mailto:?subject=&body=:%20"  												   title="Afrik Eveil" 			onclick="window.open('mailto:?subject=' + encodeURIComponent(document.title) + '&body=' + encodeURIComponent(document.URL)); return false;"><i class="fa fa-envelope-o"></i></a>
												</div>
											</div>
										</div>
									</div>
									<img class="div-line" src="../commun/images/uploads/div-line.png" alt="divide">
									<!-- paniagtion -->
									<div class="panigation2">
										<div class="row">
											<?php if($id == 1){
												$id1 = $id + 1;
												$id2 = $id + 2;
												//les prochains posts
												$next1 = $db->query("SELECT * FROM blog WHERE id='$id1'");
												$res1  = $next1->fetch_assoc();

												$next2 = $db->query("SELECT * FROM blog WHERE id='$id2'");
												$res2  = $next2->fetch_assoc();

											?>
											<div class="col-md-6  col-sm-6 col-xs-12 left">
												<div class="infor">
													<h2><a href="#"><?php $title = $res1['title'];
																		  echo substr($title, 0, 80)."...";?></a></h2>
													<div class="icon">
														<a href="singleBlog.php?post=<?php echo $res1['id']; ?>">Read</a>
													</div>
												</div>
											</div>
											<div class="col-md-6 col-sm-6 col-xs-12 right">
												<div class="infor">
													<h2><a href="#"><?php  $title = $res2['title'];
																		  echo substr($title, 0, 80)."...";?></a></h2>
													<div class="icon">
														<a href="singleBlog.php?post=<?php echo $res2['id']; ?>">Read</a>
													</div>
												</div>
											</div>
											<?php }
											//Pas le premier post
											else if($id > 1){
												$id1 = $id + 1;
												$id2 = $id - 1;
												//prochain post
												$next = $db->query("SELECT * FROM blog WHERE id='$id1'");
												$res1 = $next->fetch_assoc();
												//precedent post
												$prev = $db->query("SELECT * FROM blog WHERE id='$id2'");
												$res2 = $prev->fetch_assoc();

												?>
													<div class="col-md-6  col-sm-6 col-xs-12 left">
														<div class="infor">
															<h2><a href="#"><?php $title = $res2['title'];
																				  echo substr($title, 0, 80)."...";?></a></h2>
															<div class="icon">
																<a href="singleBlog.php?post=<?php echo $res2['id']; ?>">Read</a>
															</div>
														</div>
													</div>
													<div class="col-md-6 col-sm-6 col-xs-12 right">
														<div class="infor">
															<h2><a href="#"><?php $title = $res1['title'];
															echo substr($title, 0, 80)."...";?></a></h2>
															<div class="icon">
																<a href="singleBlog.php?post=<?php echo $res1['id'];?>">Read</a>
															</div>
														</div>
													</div>
												<?php }
											//Dernier post
											else if($count == $id){
												$id1 = $id - 1;
												$id2 = $id - 2;
												//precedent post
												$prev1 = $db->query("SELECT * FROM blog WHERE id='$id1'");
												$res1  = $prev1->fetch_assoc();
												//avant avant dernier post
												$prev2 = $db->query("SELECT * FROM blog WHERE id='$id2'");
												$res2  = $prev2->fetch_assoc();

												?>
													<div class="col-md-6  col-sm-6 col-xs-12 left">
														<div class="infor">
															<h2><a href="#"><?php $title = $res1['title'];
																				  echo substr($title, 0,80)."...";?></a></h2>
															<div class="icon">
																<a href="singleBlog.php?post=<?php echo $res2['id']; ?>">Read</a>
															</div>
														</div>
													</div>
													<div class="col-md-6 col-sm-6 col-xs-12 right">
														<div class="infor">
															<h2><a href="#"><?php $title = $res2['title'];
																				  echo substr($title, 0,80)."...";?></a></h2>
															<div class="icon">
																<a href="singleBlog.php?post=<?php echo $res1['id']; ?>">Read</a>
															</div>
														</div>
													</div>
												<?php } ?>
										</div>
									</div>

									<img class="div-line" src="../commun/images/uploads/div-line.png" alt="divide">
									<!-- comment -->
									<div class="comment">
										<div id="disqus_thread"></div>
											<div id="disqus_thread"></div>
												<script>
													(function() { // DON'T EDIT BELOW THIS LINE
													var d = document, s = d.createElement('script');
													s.src = 'https://http-188-226-149-64-afrikeveil.disqus.com/embed.js';
													s.setAttribute('data-timestamp', +new Date());
													(d.head || d.body).appendChild(s);
													})();
												</script>
												<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include_once("librairies/footer.php");?>
<?php include_once("librairies/scripts.php");?>
<script>
$(window).scrollPress();
</script>
</body>
</html>