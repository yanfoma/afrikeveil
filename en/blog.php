<?php
	include("../commun/config.php");
	include("../commun/db.php");
	include("../commun/function.php");
	$query = "SELECT * FROM blog WHERE language='en' ORDER BY id ASC";
	$posts = $db->query($query);
?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7 no-js" lang="en-US">
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8 no-js" lang="en-US">
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html lang="en" class="no-js">
<head>
	<!-- Basic need -->
	<title>BLOG | Afrik Eveil Foundation</title>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="">
	<link rel="profile" href="#">
	<?php include_once("librairies/header.php");?>
</head>

<body>
<?php include_once("librairies/menu.php");?>
<div class="hero">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1>Blog</h1>
				<img src="../commun/images/uploads/hero-line.png" alt="">
				<ul class="breadcumb">
					<li><a href="index.php">HOME</a></li>
					<li><span>/</span>BLOG</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="bloglistpost-v1 blogv3 blog servicesingle bloglistpost-v2 bloglistpost-v3">
	<div class="container">
		<div class="row">
			<?php if($posts->num_rows > 0){
						while($row = $posts->fetch_assoc()){
					?>
			<div class="col-md-4 col-sm-6 col-xs-12">
				<div class="blogpost-v1">
					<div class="blog-it normal">
						<div class="post-thumbnail">
							<a href="#"><img class="post-img" src="../admin/uploads/<?php echo $row['image'];?>" alt="thumb1"></a>
						</div>
						<div class="blog-it-left">
							<div class="row">
								<div class="col-md-3 col-sm-4 col-xs-12">
									<div class="date">
										<h1><?php
												$date=$row['date'];
												getMonthDay($date);?></h1></h1>
										<img src="../commun/images/uploads/white-line.png" alt="">
										<p><?php
												$date=$row['date'];
												getMonthEn($date);?></p>
									</div>
								</div>
								<div class="col-md-9 col-sm-8 col-xs-12">
									<div class="inner-ct">
										<h2 class="v3"><a href="singleBlog.php?post=<?php echo $row['id']; ?>">
											<?php $title = $row['title'];echo substr($title, 0, 40)."...";?></a>
										</h2>
										<div class="date-inner">
											<span><i class="ion-android-person"></i><em>By</em> <?php echo $row['author'];?></span>
											<span><i class="fa fa-calendar" aria-hidden="true"></i><em>
												<?php
													echo $row['date'];
												?>
											</span>
										</div>
										<img class="divide2" src="../commun/images/uploads/shortblog.png" alt="" width="100">
									</div>
								</div>
								<div class="inner-ct ">
									<p style="text-align:justify;"><?php
										$body = $row['body'];
										echo substr($body, 0, 250)."...";?></p>
										<a class="read" href="singleBlog.php?post=<?php echo $row['id']; ?>">Read More</a>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php }}?>
		</div>
		<!--div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<ul class="pagination">
					<p>page: </p>
					<li class="num active"><a href="#">01</a></li>
					<li class="num"><a href="#">02</a></li>
					<li class="num2"><a href="#">...</a></li>
					<li class="num"><a href="#">30</a></li>
					<li class="icon"><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
				</ul>
			</div>
		</div-->
	</div>
</div>
<?php include_once("librairies/footer.php");?>
<?php include_once("librairies/scripts.php");?></body>
<script>
$(window).scrollPress();
</script>
</html>