<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7 no-js" lang="en-US">
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8 no-js" lang="en-US">
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html lang="en" class="no-js">
<head>
	<!-- Basic need -->
	<title>How We WORK | Afrik Eveil Foundation</title>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="">
	<link rel="profile" href="#">
	<?php include_once("librairies/header.php");?>
	<style type="text/css">
		.services .item h2{

			font-size:40px;
		}

		.services .item p{

			text-align: justify;
		}

	</style>
</head>

<body>
<?php include_once("librairies/menu.php");?>
<div class="hero">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1>How We WORK</h1>
				<img src="../commun/images/uploads/hero-line.png" alt="">
				<ul class="breadcumb">
					<li><a href="index.php">HOME</a></li>
					<li><span>/</span>How We WORK</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="chooseus">
	<div class="container">
		<div class="row">
			<div class="heading-it">
				<h1>Our Values <span><br><br> Quality - Integrity - Social Value</span></h1>
				<img src="../commun/images/uploads/title-line.png" alt="">
				<p style="font-size:19px;">
					<br><br>We offer creative solutions to business-startups, policy and civil society on entrepreneurial challenges through:
				</p>
			</div>
		</div>
	</div>
</div>
<div class="services">
	<div class="container">
		<div class="row">
			<div id="service-testimonial">
				<div class="sv-item item">
					<h2>Research</h2>
					<p>Our team of experts based in Ouagadougou and Geneva investigate emerging issues and provide independent evidence, analysis and insights on topics at the intersection of public policy, entrepreneurship and business development to stakeholders and clients.
					   The results of our research form the basis of our advisory services and our experts are carefully selected based on their area of competence to optimize their skills and confidently handle projects.
					   We believe placing reliable, accessible and useful information in the right hands in a timely manner, engages citizens, influences policy alternatives and enhance business decisions.
					</p>
				</div>
				<div class=" sv-item item ">
					<h2>Advisory</h2>
					<p>With a focus on actionable solutions, we offer strategic and evidence-based advice to policy, international organizations and civil society on addressing unemployment through entrepreneurship and productivity-enhancing education.
					   In an era of perpetual flood of information and conflicting opinions, we facilitate decision making through rigorous cost-benefits analysis of options and present decision makers with workable solutions.
					   We also offer efficient communication strategies and innovation fundraising approaches to stakeholders and clients.
					</p>
				</div>
				<div class=" sv-item item ">
					<h2>Incubation</h2>
					<p>We foster entrepreneurial and start-up development in West Africa through provision of management training, coaching, mentoring, networking and social media strategies.
					We are motivated by our firm conviction that innovative ideas implemented with business acumen is key to addressing poverty and reducing unemployment among young people West Africa
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include_once("librairies/footer.php");?>
<?php include_once("librairies/scripts.php");?>
<script>
$(window).scrollPress();
</script>
</body>
</html>