<?php
	include("../commun/config.php");
	include("../commun/db.php");
	include("../commun/function.php");
	if(isset($_POST['subscribe'])){
		$name  = mysqli_real_escape_string($db, $_POST['name']);
		$email = mysqli_real_escape_string($db, $_POST['email']);
		$query = "SELECT count(*) FROM subscribers WHERE email='$email'";

		$checkEmail = $db->query($query);
		if($checkEmail ==1 ){
			echo"<script> alert('You have already subscribed to our Newsletter');</script>";
			header("location:index.php");
		}else{
			$query ="INSERT INTO subscribers (name,email) VALUES('$name', '$email')";
			$db->query($query);
			echo"<script> alert('You have been successfully registered !!!');</script>";
			header("location:index.php");
		}
	}

	$query_posts  ="SELECT * FROM posts WHERE language='en' ORDER BY id ASC LIMIT 2";
	$lastest_post =$db->query($query_posts);


	$query_blog   ="SELECT * FROM blog WHERE language='en' ORDER BY id ASC LIMIT 3";
	$lastest_blog =$db->query($query_blog);

?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7 no-js" lang="en-US">
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8 no-js" lang="en-US">
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html lang="en" class="no-js">
<head>
	<!-- Basic need -->
	<title>Afrik Eveil Foundation</title>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="">
	<link rel="profile" href="#">
	<?php include_once("librairies/header.php");?>
</head>

<body>
<?php include_once("librairies/menu.php");?>
<?php include_once("librairies/slideShow.php");?>
<br/><br/><br/><br/>
<div class="aboutservice">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="heading-it">
					<h1>The Afrik Eveil Foundation</h1>
					<img src="../commun/images/uploads/line-title.png" alt="">
					<p class="afIntro">
						Afrik Eveil is a not-for-profit organization created and managed by experienced professionals
					    living in Burkina Faso and abroad;
					    who are enthusiastic about promoting entrepreneurship as a means of addressing unemployment challenges in West Africa.
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include_once("librairies/latest.php");?>
<?php include_once("librairies/videoIntro.php");?>
<?php include_once("librairies/newsletter.php");?>
<?php include_once("librairies/footer.php");?>
<?php include_once("librairies/scripts.php");?>
    <!-- Html 5 light box script-->
<script src="../commun/js/html5lightbox/html5lightbox.js"></script>
<script>
$(window).scrollPress();
</script>
</body>
</html>