<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7 no-js" lang="en-US">
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8 no-js" lang="en-US">
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html lang="en" class="no-js">
<head>
	<!-- Basic need -->
	<title>Contact | Afrik Eveil Foundation</title>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="">
	<link rel="profile" href="#">
	<?php include_once("librairies/header.php");?>
	<style>
#map {
  height: 400px;
  margin-bottom: 20px;
}

.active {
  max-width: 150px;
  background: #099;
  color: #fff;
}
</style>
</head>

<body>
<?php include_once("librairies/menu.php");?>
<div class="hero">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1>CONTACT US </h1>
				<img src="../commun/images/uploads/hero-line.png" alt="">
				<ul class="breadcumb">
					<li><a href="index.php">HOME</a></li>
					<li><span>/</span> Contact Us</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="contactpage servicesingle map-contact" id="map-check">
	<div class="container">
		<div class="row">
			<div class="contact-it sidebar">
				<div class="col-md-4 col-sm-12 col-xs-12">
					<div class="sb-it">
						<div class="sb-title">
							<h2>Our Contacts</h2>
						</div>
						<div class="sb-content">
							<div class="ct-it">
								<i class="fa fa-map-marker" aria-hidden="true"></i>
								<p>Cité Azimmo Ouaga 2000, 111 BP 1597 Ouagadougou CMS 11—Burkina Faso</p>
							</div>
							<div class="ct-it">
								<i class="ion-android-phone-portrait"></i>
								<p>  +226 25 66 64 60</p>

							</div>
							<div class="ct-it">
								<i class="ion-email"></i>
								<p><a href="#">info@afrikeveil.org</a><br/>
									<a href="#">Ghana:ekumi@afrikeveil.org</a>
								</p>
							</div>
							<div class="ct-icon">
								<a href="https://www.facebook.com/afrikeveil.org/" target="_blank" class="hvr-grow"><i class="ion-social-facebook "></i></a>
								<a href="https://twitter.com/a_eveil" 			   target="_blank" class="hvr-grow"><i class="ion-social-twitter"></i></a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-8 col-sm-12 col-xs-12">
					<!--div id="map_canvas" style="width:100%;height:430px">
					</div-->
				  <div id="map"></div>
					<div id="dragend"></div>
					<div id="bounds_changed"></div>
				  	<script src='https://maps.googleapis.com/maps/api/js'></script>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="contactv1">
	<div class="container contact-ct">
		<div class="row">
			<div class="col-md-9 col-sm-8 col-xs-12">
				<h1>Send us a message on Facebook Messenger</h1>
			</div>
			<div class="col-md-3 col-sm-4 col-xs-12">
				<div class="contact-bt">
					<a href="https://m.me/afrikeveil.org" target="_blank" class="readmore2">Send</a>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include_once("librairies/footer.php");?>
<?php include_once("librairies/scripts.php");?>
<script>
$(window).scrollPress();
</script>
</body>
</html>