<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7 no-js" lang="en-US">
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8 no-js" lang="en-US">
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html lang="en" class="no-js">
<head>
	<!-- Basic need -->
	<title>ARE YOU INTERESTED? | Afrik Eveil Foundation</title>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="">
	<link rel="profile" href="#">
	<?php include_once("librairies/header.php");?>
</head>

<body>
<?php include_once("librairies/menu.php");?>
<div class="hero">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1>ARE YOU INTERESTED?</h1>
				<img src="../commun/images/uploads/hero-line.png" alt="">
				<ul class="breadcumb">
					<li><a href="index.php">HOME</a></li>
					<li><span>/</span>ARE YOU INTERESTED?</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="chooseus chooseusv2 ">
	<div class="container">
		<div class="row">
			<div class="heading-it">
				<h1>ARE YOU <span> INTERESTED?</span></h1>
				<img src="../commun/images/uploads/title-line.png" alt="">
			</div>
		</div>

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="chooseus-it">
					<img src="../commun/images/icon1.png" alt="">
					<div class="step-content">
						<h2>Become a Patron</h2>
						<p>Are you an expert, experienced consultant, coach, or successful businessperson enthusiastic about youth employment and entrepreneurship in Burkina Faso/West Africa?
						   You can support us with your expertise and knowledge in business management training, networking and nurturing our members and clients.
						</p>
					</div>
				</div>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="chooseus-it">
					<img src="../commun/images/icon2.png" alt="">
					<div class="step-content">
						<h2>Become a Member</h2>
						<p>Are you a young Burkinabe  or West African living or studying  abroad with an entrepreneurial idea,  initiative or  specific solution for youth unemployment and education?
						   Do you have a fervent commitment to serve as an inspiring agent to other young Burkinabe in contributing to national development?
 						   If yes, then join us by applying to the B.E. Foundation. Fill our membership form and send us your cv along with your project. We provide support platform to aid in the advancement of your initiative or idea.
						</p>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="chooseus-it">
					<img src="../commun/images/icon1.png" alt="">
					<div class="step-content">
						<h2>Become a Sponsor</h2>
						<p>We offer the possibility to public, private, regional and international organizations to support our projects and initiatives through grants, donations and capacity building.
						 </p>
					</div>
				</div>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="chooseus-it">
					<img src="../commun/images/icon6.png" alt="">
					<div class="step-content">
						<h2>Partner with us</h2>
						<p>We are keen on partnering with research organizations, consultancy firms, advocacy organizations, private foundations, regional and international organizations  and other civil society organizations on projects that align with our organizational goals.
						</p>
					</div>
				</div>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="chooseus-it">
					<img src="../commun/images/icon5.png" alt="">
					<div class="step-content">
						<h2>Become a Client</h2>
						<p>Are you a foundation, civil society or international organization working on entrepreneurship and youth employment and entrepreneurship and looking for first hand data, evidence and local partnership? You can become our client. We deliver your needs with speed and precision.
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!--div class="contactform consult">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-sm-12 col-xs-12">
				<div class="test-title">
					<h2>Contactez - Nous!</h2>
					<img  src="../commun/images/uploads/line-title.png" alt="">
					<p></p>
				</div>
			</div>
			<div class="col-md-8 col-sm-12 col-xs-12">
				<div class="left-it ct-form">
					<form class="ct-left">
						<div class="row">
							<div class="col-md-6 col-sm-12 col-xs-12">
								<h2>Je voudrais :</h2>
								<select>
									<option value="DevenirParrain">Devenir Parrain</option>
									<option value="DevenirMemmbre">Devenir Memmbre</option>
									<option value="DevenirPartenaire">Devenir Partenaire</option>
									<option value="DevenirClient">Devenir Client</option>
								</select>
							</div>
							<div class="col-md-6 col-sm-12 col-xs-12">
								<h2>Nom & Prénoms</h2>
								<input class="name" type="text" placeholder="Nom et prénoms">
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-sm-12 col-xs-12">
								<h2>Numéro De Téléphone(*)</h2>
								<input class="phone" type="text" placeholder="Votre numéro de téléphone">
							</div>
							<div class="col-md-6 col-sm-12 col-xs-12">
								<h2>Email</h2>
								<input class="phone" type="email" placeholder="Votre email">
							</div>
							<div class="col-md-6 col-sm-12 col-xs-12">
								<h2>Nous vous répondrons aussitôt que possible!</h2>
								<div>
									<input class="submit" type="submit" name="submit" value="Envoyer">
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div-->
<?php include_once("librairies/footer.php");?>
<?php include_once("librairies/scripts.php");?>
<script>
$(window).scrollPress();
</script>
</body>
</html>