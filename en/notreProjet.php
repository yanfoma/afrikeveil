<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7 no-js" lang="en-US">
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8 no-js" lang="en-US">
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html lang="en" class="no-js">
<head>
	<!-- Basic need -->
	<title>Our Project | Afrik Eveil Foundation</title>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="">
	<link rel="profile" href="#">
	<?php include_once("librairies/header.php");?>
</head>

<body>
<?php include_once("librairies/menu.php");?>
<div class="hero">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1>Our Project  </h1>
				<img src="../commun/images/uploads/hero-line.png" alt="">
				<ul class="breadcumb">
					<li><a href="index.php">HOME</a></li>
					<li><span>/</span> Our Project</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="servicesv3">
	<div class="container">
		<div class="row">
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="heading-it">
					<h1>Our Project</h1>
					<img src="../commun/images/uploads/line-title2.png" alt="">
					<p><br> View All</p>
				</div>
			</div>
			<div class="col-md-9 col-sm-6 col-xs-12">
				<div id="owl-servicev3">
					<div class="sv-item item it1">
						<div class="hover-inner">
							<p>Afrik Eveil is an organization of young professionals and Burkinabè students from the diaspora and within the country who have voluntarily chosen to engage themselves in the promotion of social entrepreneurship as a means of fighting unemployment of young graduates in Burkina Faso .</p>
						</div>
					</div>
					<div class="sv-item item it2">
						<div class="hover-inner">
							<p>Our mission is to inculcate the young graduates the entrepreneurial spirit on the one hand and on the other hand to engage more Burkina Faso's diaspora in the projects of development of the mother country.</p>
						</div>
					</div>
					<div class="sv-item item it2">
						<div class="hover-inner">
							<p>We also work to build a wide network of contacts (Burkina Faso, Taiwan, USA, Canada, France, Switzerland, Morocco, Italy ...) in order to provide opportunities for those who want to meet the challenges of the moment.</p>
						</div>
					</div>
					<div class="sv-item item it2">
						<div class="hover-inner">
							<p>Our vision is to be the organization of reference in the promotion of young entrepreneurship and innovation in Burkina Faso.</p>
						</div>
					</div>
					<div class="sv-item item it2">
						<div class="hover-inner">
							<p>In order to achieve our goals, we designed the Young Entrepreneurship Empowerment Roadmap (YEER), a series of activities meticulously implemented to benefit "young entrepreneurs".</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include_once("librairies/footer.php");?>
<?php include_once("librairies/scripts.php");?>
<script>
$(window).scrollPress();
</script>
</body>
</html>