<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7 no-js" lang="en-US">
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8 no-js" lang="en-US">
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html lang="en" class="no-js">
<head>
	<!-- Basic need -->
	<title>OUR ORGANIZATION | Afrik Eveil Foundation</title>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="">
	<link rel="profile" href="#">
	<?php include_once("librairies/header.php");?>
	<style type="text/css">
		.tab p {
			text-align: justify;
		}
	</style>
</head>

<body>
<?php include_once("librairies/menu.php");?>
<div class="hero">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1>Our Organization</h1>
				<img src="../commun/images/uploads/hero-line.png" alt="">
				<ul class="breadcumb">
					<li><a href="index.php">HOME</a></li>
					<li><span>/</span>WHO WE ARE</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="intro clientsv3">
	<div class="container">
		<div class="intro-ct">
			<div class="row">
				<div class="col-md-6 col-sm-12 col-xs-12">
					<div id="owl-intro">
						<div class="intro-it item">
							<img src="../commun/images/uploads/intro-slider.png" alt="">
						</div>
					</div>
				</div>
				<div class="col-md-6 col-sm-12 col-xs-12">
					<img class="quote-ico" src="../commun/images/uploads/quote-ico.png" alt="">
					<div class="clientv3-it">
						<p>Our mission is to connect academia and industry while supporting young entrepreneurs in starting and scaling-up their businesses to reduce unemployment among youth.</p>
						<a href="#">---  Damien Somé - Chairman/ Founder</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="projectv3">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-12 col-xs-12">
				<div class="video">
					<img  class="thumbnail" src="../commun/images/uploads/projectv3-img.png" alt="">
					<a class="html5lightbox thm-btn" title="Introduction DE AFRIK EVEIL" href="https://youtu.be/mObSx7bMNTE"><img class="btn hvr-grow" src="../commun/images/uploads/playbtn1.png" alt=""></a>
				</div>
			</div>
			<div class="col-md-6 col-sm-12 col-xs-12">
				<div class="project-left">
					<div class="heading-it">
						<h1>The Afrik Eveil Foundation</h1>
						<img src="../commun/images/uploads/line-title2.png" alt="">
						<p style="text-align:justify;">
							Afrik Eveil is a not-for-profit organization created and managed by experienced professionals
					    living in Burkina Faso and abroad;
					    who are enthusiastic about promoting entrepreneurship as a means of addressing unemployment challenges in West Africa.
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="aboutservice">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="heading-it">
					<p>WE ARE A OUAGADOUGOU AND GENEVA-BASED NONPROFIT THINK TANK DEDICATED TO ENTREPRENEURSHIP IN WEST AFRICA</p>
					<img src="../commun/images/uploads/line-title2.png" alt="">
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="tabs">
					<ul class="tab-links">
						<li class="active"><a href="#tab1"> MISSION</a></li>
						<li><a href="#tab2">VISION</a></li>
						<li><a href="#tab3">OUR APPROACH</a></li>
					</ul>
				    <div class="tab-content">
				        <div id="tab1" class="tab active">
				            <div class="row">
				            	<div class="col-md-4 col-sm-12 col-xs-12">
				            		<img src="../commun/images/uploads/mission.png" alt="">
				            	</div>
				            	<div class="col-md-8 col-sm-12 col-xs-12">
				            		<h2><a href="#"></a></h2>
				            		<p>Our mission is to connect academia and industry while supporting young entrepreneurs in starting and scaling-up their businesses to reduce unemployment among youth.
									</p>
				            	</div>
				            </div>
				        </div>
				        <div id="tab2" class="tab ">
				           <div class="row">
				            	<div class="col-md-4 col-sm-12 col-xs-12">
				            		<img src="../commun/images/uploads/vision.png" alt="">
				            	</div>
				            	<div class="col-md-8 col-sm-12 col-sm-12">
				            		<h2><a href="#"></a></h2>
				            		<p>Our vision is to become a leading and trusted knowledge-based organization fostering innovative ideas and  young entrepreneurs in Africa.</p>
				            	</div>
				            </div>
				        </div>
				        <div id="tab3" class="tab">
				        	<div class="row">
				            	<div class="col-md-4 col-sm-12 col-xs-12">
				            		<img src="../commun/images/uploads/approche.png" alt="">
				            	</div>
				            	<div class="col-md-8 col-sm-12 col-sm-12">
				            		<h2><a href="#"></a></h2>
				            		<p>We combine applied research with analytical and technical competence to fill data, quality research and strategic advisory gaps on entrepreneurship in Africa.
									   We are also currently growing a diverse and global support network, gathering individuals from Burkina Faso, Ghana, Taiwan, USA, Canada, France, Switzerland, Morocco and Italy in order to provide the a balanced global perspective needed by budding entrepreneurs.
									   In order to reach our goals, we look forward to implementing the Afrik Eveil Young Entrepreneurship Empowerment Roadmap (YEER), a program designed to support the needs of nascent entrepreneurs.
									</p>
				            	</div>
				            </div>
			       	 	</div>
				    </div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include_once("librairies/footer.php");?>
<?php include_once("librairies/scripts.php");?>
    <!-- Html 5 light box script-->
<script src="../commun/js/html5lightbox/html5lightbox.js"></script>
</body>
</html>