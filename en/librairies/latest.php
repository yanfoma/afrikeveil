<div class="blog" style="background: #ea9818;">
	<div class="container" >
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="heading-it">
				<h1>OUR LATEST BLOGS</h1>
				<img src="../commun/images/uploads/title-line.png" alt="">
			</div>
			</div>
		</div>
		<div class="row">
			<?php while($row_blog = $lastest_blog->fetch_assoc()){?>
			<div class="col-md-4 col-sm-12 col-xs-12"  >
				<div class="blog-it-left">
					<div class="post-thumbnail">
						<a href="#"><img class="blog-img" src="../admin/uploads/<?php echo $row_blog['image'];?>" alt=""></a>
						<div class="date">
							<span><i class="fa fa-calendar" aria-hidden="true"></i>
								<?php $date=$row_blog['date'];getMonthEn($date);echo " ";getMonthYear($date)?>
							<span>|</span>
							<span>
							<?php echo $row_blog['author'];?></span>

							<span></span>
						</div>
					</div>
					<div class="blog-ct-left">
						<h2><a href="singleBlog.php?post=<?php echo $row_blog['id']; ?>">
											<?php $title = $row_blog['title'];echo substr($title, 0, 40)."...";?></a></h2>
						<p style="text-align:justify;"><?php
							$body = $row_blog['body'];
							echo substr($body, 0, 200)."...";?></p>
							<a class="read" href="singleBlog.php?post=<?php echo $row_blog['id']; ?>">Read More</a>
						</p>
					</div>
				</div>
			</div>
			<?php }?>
		</div>
	</div>
</div>