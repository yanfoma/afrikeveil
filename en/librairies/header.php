
	<!-- Google Fonts-->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Poppins:400,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,700,300i" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
	<!-- Latest compiled and minified CSS -->
    <!-- CSS files -->
    <link rel="stylesheet" href="../commun/css/plugins.css">
    <link rel="stylesheet" href="../commun/css/style.css">
    <!--polyglot CSS -->
    <link href="../commun/css/polyglot-language-switcher.css" type="text/css" rel="stylesheet">
    <!--scrollPress CSS -->
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="../commun/css/scrollPress.css" rel="stylesheet" type="text/css">
