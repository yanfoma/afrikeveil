<div class="bloglistpost-v1 blogv3 blog servicesingle bloglistpost-v2 bloglistpost-v3">
	<div class="container">
		<div class="row">
			<?php while($row = $lastest_post->fetch_assoc()){?>
			<div class="col-md-6 col-sm-12 col-xs-12">
				<div class="blogpost-v1">
					<div class="blog-it normal2">
						<div class="post-thumbnail">
							<a href="#"><img class="post-img" src="../admin/uploads/<?php echo $row['image']?>" alt="thumb1" style="height:auto;"></a>
						</div>
						<div class="blog-it-left">
							<div class="row">
								<div class="col-md-2 col-sm-2 col-xs-12">
									<div class="date">
										<h1><?php
												$date=$row['date'];
												getMonthDay($date);?></h1>
										<img src="../commun/images/uploads/white-line.png" alt="">
										<p><?php
												$date=$row['date'];
												getMonthEn($date);?></p>
									</div>
								</div>
								<div class="col-md-10 col-sm-10 col-xs-12">
									<div class="inner-ct">
										<h2><a href="single.php?post=<?php echo $row['id']; ?>">
											<?php $title = $row['title'];echo substr($title, 0, 40)."...";?></a>
										</h2>
										<div class="date-inner">
											<span><i class="ion-android-person"></i><em>By </em><?php echo $row['author'];?></span>
											<span>|</span>
											<span><i class="fa fa-calendar" aria-hidden="true"></i>
												<?php
													echo $row['date'];
												?>
											</span>
										</div>
										<img class="divide2" src="../commun/images/uploads/blogline.png" alt="">
										<p style="text-align:justify;"><?php
											$body = $row['body'];
											echo substr($body, 0, 300)."...";?></p>
											<a class="read" href="single.php?post=<?php echo $row['id']; ?>">Read More</a>
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
			<?php }?>
		</div>
		<!--div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<ul class="pagination">
					<p>page: </p>
					<li class="num active"><a href="#">01</a></li>
					<li class="num"><a href="#">02</a></li>
					<<li class="num2"><a href="#">...</a></li>
					<li class="num"><a href="#">30</a></li>>
					<li class="icon"><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
				</ul>
			</div>
		</div-->
	</div>
</div>

<div class="blog" style="background: #5bbb2d;">
	<div class="container" >
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="heading-it">
				<h1>OUR LATEST BLOGS</h1>
				<img src="../commun/images/uploads/title-line.png" alt="">
			</div>
			</div>
		</div>
		<div class="row">
			<?php while($row_blog = $lastest_blog->fetch_assoc()){?>
			<div class="col-md-4 col-sm-12 col-xs-12"  >
				<div class="blog-it-left">
					<div class="post-thumbnail">
						<a href="#"><img class="blog-img" src="../admin/uploads/<?php echo $row_blog['image'];?>" alt=""></a>
						<div class="date">
							<span><i class="fa fa-calendar" aria-hidden="true"></i>
								<?php $date=$row_blog['date'];getMonthEn($date);echo " ";getMonthYear($date)?>
							<span>|</span>
							<span><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
							<?php echo $row_blog['author'];?></span>

							<span></span>
						</div>
					</div>
					<div class="blog-ct-left">
						<h2><a href="singleBlog.php?post=<?php echo $row_blog['id']; ?>">
											<?php $title = $row_blog['title'];echo substr($title, 0, 40)."...";?></a></h2>
						<p style="text-align:justify;"><?php
							$body = $row_blog['body'];
							echo substr($body, 0, 200)."...";?></p>
							<a class="read" href="singleBlog.php?post=<?php echo $row_blog['id']; ?>">Read More</a>
						</p>
					</div>
				</div>
			</div>
			<?php }?>
		</div>
	</div>
</div>