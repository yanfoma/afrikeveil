<footer class="ht-footer">
	<div class="container">
		<div class="row">
			<div class="col-md-3 col-sm-12 col-xs-12">
				<div class="ft-contact">
					<div class="ft-heading">
						<h6>About Us</h6>
					</div>
					<div class="ct-it">
						<p  style="text-align:left; color: #888888;">AFRIK EVEIL fosters start-up development and the scaling-up of innovative and sustainable business solutions in the West African region. We combine applied research with analytical and technical competence of our diverse team of experts, to provide strategic support to small and medium enterprises through provision of management training, coaching and mentoring, networking and social media strategies.
						</p>
					</div>
				</div>
			</div>
			<div class="col-md-6 col-sm-12 col-xs-12">
				<div class="ft-infor">
					<div class="ft-heading">
						<h6>AfrikEveil Sitemap</h6>
					</div>
					<div class="infor-it">
						<ul class="left">
							<li><i class="fa fa-angle-right" aria-hidden="true"></i><a href="index.php">Home</a></li>
							<li><i class="fa fa-angle-right" aria-hidden="true"></i><a href="notreOrganisation.php">Our Organization</a></li>
							<li><i class="fa fa-angle-right" aria-hidden="true"></i><a href="notreHistoire.php">Our History</a></li>
							<li><i class="fa fa-angle-right" aria-hidden="true"></i><a href="notreEquipe.php">Our Team</a></li>
							<li><i class="fa fa-angle-right" aria-hidden="true"></i><a href="cequeNouFaisons.php">How We Work</a></li>
							<li><i class="fa fa-angle-right" aria-hidden="true"></i><a href="estVousInterrese.php">Are you interested ?</a></li>
						</ul>
						<ul class="left">
							<li><i class="fa fa-angle-right" aria-hidden="true"></i><a href="nosactualites.php">Our News</a></li>
							<li><i class="fa fa-angle-right" aria-hidden="true"></i><a href="blog.php">Blog</a></li>
							<li><i class="fa fa-angle-right" aria-hidden="true"></i><a href="contact.php">Contact</a></li>
							<li><i class="fa fa-angle-right" aria-hidden="true"></i><a href="ressources.php">Resources</a></li>
							<li><i class="fa fa-angle-right" aria-hidden="true"></i><a href="discours.php">Speech at the House of Lords</a></li>

						</ul>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-12 col-xs-12">
				<div class="ft-contact">
					<div class="ft-heading">
						<h6>Our Contacts</h6>
					</div>
					<ul class="ct-it">
						<li><i class="fa fa-map-marker" aria-hidden="true"></i><a href="#">Cité Azimmo Ouaga 2000, 111 BP 1597 Ouagadougou CMS 11—Burkina Faso</a></li><br/>
						<li><i class="fa fa-envelope" aria-hidden="true"></i><a href="#">Email:  info@afrikeveil.org</a></li><br/>
						<li><i class="ion-ios-telephone"></i><a href="#">Phone: +226 25 66 64 60</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</footer>
<div class="ft-below">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="ft-below-left">
					<center><p>Afrik Eveil Foundation © 2017 All Copyrights Reserved. By <a class="readmore2"href="https://yanfoma.tech" target="_blank">Yanfoma</a>.</p></center>
				</div>
			</div>
		</div>
	</div>
</div>
