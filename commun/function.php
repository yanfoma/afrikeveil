<?php
function active($page) {
  $request_url = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
  $result = strpos($request_url,$page);
    if ($result == false) print "";
    else print "active";
}
?>
<?php
function language($lang)
{
	$currenturl = $_SERVER["REQUEST_URI"];
	switch ($lang) {
		case 'anglais':
			echo strpos($currenturl,'/fr/')?str_replace('/fr/','/en/',$currenturl):str_replace('/en/','/fr/',$currenturl);
			break;
		case 'allemand':
			echo strpos($currenturl,'/fr/')?str_replace('/fr/','/gm/',$currenturl):str_replace('/gm/','/fr/',$currenturl);
			break;
		case 'Englisch':
			echo strpos($currenturl,'/gm/')?str_replace('/gm/','/en/',$currenturl):str_replace('/en/','/gm/',$currenturl);
			break;
		case 'Französisch':
			echo strpos($currenturl,'/gm/')?str_replace('/gm/','/fr/',$currenturl):str_replace('/fr/','/gm/',$currenturl);
			break;
		case 'francais':
			echo strpos($currenturl,'/en/')?str_replace('/en/','/fr/',$currenturl):str_replace('/fr/','/en/',$currenturl);
			break;
		case 'german':
			echo strpos($currenturl,'/en/')?str_replace('/en/','/gm/',$currenturl):str_replace('/gm/','/en/',$currenturl);
			break;
		default:
			header ("Location: $currenturl");
			break;
	}
	//echo strpos($currenturl,'/en/')?str_replace('/en/','/fr/',$currenturl):str_replace('/fr/','/en/',$currenturl);
}
function comments($count){
 	if($count == 0) echo "Pas de commentaire";
 	else if($count == 1) echo "1$count commentaire";
 	else if($count < 10 && $count!=0) echo "0$count commentaires";
 	else echo "$count Commentaires";
}

function getMonth($date){
		$month = explode(" ", $date);
		switch ($month[0]) {
			case 'January':
				echo "Janvier";
				break;
			case 'February':
				echo "Fevrier";
				break;
			case 'March':
				echo "Mars";
				break;
			case 'April':
				echo "Avril";
				break;
			case 'May':
				echo "Mai";
				break;
			case 'June':
				echo "Juin";
				break;
			case 'July':
				echo "Juillet";
				break;
			case 'August':
				echo "Aôut";
				break;
			case 'September':
				echo "Septembre";
				break;
			case 'October':
				echo "Octobre";
				break;
			case 'November':
				echo "Novembre";
				break;
			case 'December':
				echo "Décembre";
				break;

			default:
				return $month;
				break;
		}
	}

	function getMonthEn($date){
		$month = explode(" ", $date);
		echo $month[0];
	}

	function getMonthDay($date){
		$month = explode(" ", $date);
		echo $month[1];
	}
	function getMonthYear($date){
		$month = explode(" ", $date);
		echo $month[2];
	}
?>
