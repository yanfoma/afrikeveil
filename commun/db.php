<?php

	$db = new mysqli (DB_HOST, DB_USER, DB_PASS, DB_NAME);
	if ($db->connect_error) {
	    die("Connection failed: " . $db->connect_error);
	}

	$db->query('SET NAMES utf8');
?>