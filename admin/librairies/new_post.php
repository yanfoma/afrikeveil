<?php
  include_once("../commun/config.php");
  include_once("../commun/db.php");

  $valid_formats = array("jpg", "png", "gif", "zip", "bmp");
  $max_file_size = 1024*10000; //10000 kb
  $path = "uploads/"; // Upload directory
  $count = 0;
  $flag=0;

  if(isset($_POST['add_post'])){

      $title    = mysqli_real_escape_string($db, $_POST['title']);
      $author   = mysqli_real_escape_string($db, $_POST['author']);
      $body     = mysqli_real_escape_string($db, $_POST['body']);
      $language = mysqli_real_escape_string($db, $_POST['language']);

      if(empty($author)){
        $author="afrikEveil";
      }

      if(isset($_POST['id'])){
        $id     = mysqli_real_escape_string($db, $_POST['id']);
        $query  = "UPDATE posts SET title='$title',author='$author',language='$language',body='$body' WHERE id='$id' ";
      }else{
         // Loop $_FILES to exeicute all files
          foreach ($_FILES['files']['name'] as $f => $name)
            {
                if ($_FILES['files']['error'][$f] == 4)
                {
                    continue; // Skip file if any error found
                }
              if ($_FILES['files']['error'][$f] == 0)
              {
                  if ($_FILES['files']['size'][$f] > $max_file_size)
                    {
                        $message[] = "$name is too large!.";
                        continue; // Skip large files
                    }
                  elseif( ! in_array(pathinfo($name, PATHINFO_EXTENSION), $valid_formats) )
                    {
                      $message[] = "$name is not a valid format";
                      continue; // Skip invalid file formats
                    }
                  else
                    { // No error found! Move uploaded files

                        if(move_uploaded_file($_FILES["files"]["tmp_name"][$f], $path.$name))
                        {

                          $count++; // Number of successfully uploaded file
                          $d      = getDate();
                          $date   = "$d[month] $d[mday] $d[year]";
                          $image=implode(" ", $_FILES['files']['name']);
                          //$date=date("Y/m/d");
                          echo $image;
                          if($flag==0)
                            {
                              $query  = "INSERT INTO posts(title,author,body,image,language,date)
                                                VALUES('$title','$author','$body','$image','$language','$date')";
                              //mysqli_query($con,"INSERT into `panda` (name,price,description,image,currentdate) VALUES('$nom','$prix','$description','$image','$date')");
                            }
                            $flag++;
                            echo "<script>alert('Post Done!!');</script>";
                            echo "<script>window.location.href = 'index.php'</script>";
                            }



                    }
              }
          }
        /*$d      = getDate();
        $date   = "$d[month] $d[mday] $d[year]";
        $query  = "INSERT INTO posts(title,author,body,category,keywords,date)
                                  VALUES('$title','$author','$body','$category','$keywords','$date')";*/
      }

      $db->query($query);
      echo "<script>alert('Done!!');</script>";
      echo "<script>window.location.href = 'index.php'</script>";

  }
  if(isset($_GET['post'])){
    $id = mysqli_real_escape_string($db, $_GET['post']);
    $p  = $db->query("SELECT * FROM posts WHERE id ='$id' ");
    $p  = $p->fetch_assoc();

  }
  $author =$db->query("SELECT * FROM admin");
?>
<!-- Bootstrap -->
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
<link href="../commun/css/admin/fileinput/fileinput.css" media="all" rel="stylesheet" type="text/css" />
    <!-- Font Awesome -->
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<!-- piexif.min.js is only needed if you wish to resize images before upload to restore exif data.
     This must be loaded before fileinput.min.js -->
<script src="../js/fileinput/plugins/piexif.min.js" type="text/javascript"></script>

<script src="../commun/js/fileinput/fileinput.js" type="text/javascript"></script>
<script src="../commun/js/fileinput/fileinput_locale_fr.js" type="text/javascript"></script>
<script src="../commun/js/fileinput/fileinput_locale_es.js" type="text/javascript"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" type="text/javascript"></script>
<style>
#wrapper
{
 text-align:center;
 margin:0 auto;
 padding:0px;
 width:100%;
}
#output_image
{
 max-width:100%;
 margin-top: 20px;

}
</style>

<div class="row wrapper border-bottom page-heading">
  <div class="col-lg-12">
    <center><h2>Add New Post / Ajouter Un Nouvel Article</h2></center>
  </div>
  <div class="col-lg-12"> </div>
</div>
<div class="wrapper-content container ">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-content collapse in">
          <div class="widgets-container">
            <form method="post" enctype="multipart/form-data">
              <?php if(isset($p)){
                    echo "<input type='hidden' value='$id' name='id' />";
                  }
              ?>
              <div class="form-group">
                <label class="control-label">Post Title:</label>
                <input class="form-control" type="text" value="<?php echo @$p['title'];?>" name="title" />
              </div>
              <div class="form-group">
                <label control-label>Post Author:</label>
                <select class="form-control"  name="category" />
                <?php
                    while($row = $author->fetch_assoc()){
                        $selected = ($row['name'] == $p['author'] ? "selected":"");
                      ?>
                  <option value="<?php echo $row['id']?>" <?php echo $selected;?>><?php echo $row['name']?></option>
                  <?php }
                ?>
                </select>
              </div>
              <div class="form-group">
                <div class="col-md-14 ">
                  <label    class="control-label">Post Image:</label>
                  <div      class="fileinput fileinput-new input-group"id="wrapper" data-provides="fileinput">
                    <div    class="form-control" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i>
                      <span class="fileinput-filename"></span>
                    </div>
                      <span   class="input-group-addon btn btn-default btn-file">
                        <span class="fileinput-new">Select file</span>
                        <span class="fileinput-exists">Change</span>
                      <input  type="file"  name="files[]" accept="image/*" id="file" onchange="preview_image(event)" value="<?php echo @$p['image'];?>" class="file-loading">
                      </span>
                    <a href="components_fileinput.html#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>

                  </div>
                  <img id="output_image"/>
                </div>
                <script>
              $(document).on('ready', function() {
                  $("#file").fileinput({
                      showUpload: true,
                      maxFileCount: 10,
                      mainClass: "input-group-lg"
                  });
              });
              </script>
              </div>
              <div class="form-group">
                <label control-label>Post Language:</label>
                <select class="form-control"  name="language" />
                  <option value="fr">Français/French</option>
                  <option value="en">Anglais/English</option>
                </select>
              </div>
              <div class="form-group">
                <textarea name="body" class="tinymce"><?php echo @$p['body'];?></textarea>
              </div>
              <button class="btn btn-default" type="submit" name="add_post">Add Post</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script type='text/javascript'>
function preview_image(event)
{
 var reader = new FileReader();
 reader.onload = function()
 {
  var output = document.getElementById('output_image');
  output.src = reader.result;
 }
 reader.readAsDataURL(event.target.files[0]);
}
</script>