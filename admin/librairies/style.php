<!-- Bootstrap -->
	<link href="../commun/css/admin/bootstrap.min.css" 			rel="stylesheet">
	<!-- slimscroll -->
	<link href="../commun/css/admin/jquery.slimscroll.css" 		rel="stylesheet">
	<!-- Fontes -->
	<link href="../commun/css/admin/font-awesome.min.css" 		rel="stylesheet">
	<link href="../commun/css/admin/simple-line-icons.css" 		rel="stylesheet">
	<!-- all buttons css -->
	<link href="../commun/css/admin/buttons.css" 				rel="stylesheet">
	<!-- animate css -->
	<link href="../commun/css/admin/animate.css" 				rel="stylesheet">
<!-- adminbag main css -->
	<link href="../commun/css/admin/main.css" 					rel="stylesheet">
	<!-- aqua black theme css -->
	<link href="../commun/css/admin/green-blue.css" 			rel="stylesheet">
	<!-- media css for responsive  -->
	<link href="../commun/css/admin/main.media.css" 			rel="stylesheet">
  	<link href="../commun/css/admin/jasny-bootstrap.min.css"    rel="stylesheet" >
  	<!-- Fonts -->
	<link href="assets/css/font-awesome.min.css" rel="stylesheet">
	<!--[if lt IE 9]> <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script> <![endif]-->
	<!--[if lt IE 9]> <script src="dist/html5shiv.js"></script> <![endif]-->