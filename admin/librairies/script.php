
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script type="text/javascript" src="../commun/js/vendor/jquery.min.js"></script>
	<!-- bootstrap js -->
	<script type="text/javascript" src="../commun/js/vendor/bootstrap.min.js"></script>
	<!--  chartJs js  -->
	<script type="text/javascript" src="../commun/js/vendor/chartJs/Chart.bundle.js"></script>
	<!--timeline_horizontal-->
	<script type="text/javascript" src="../commun/js/vendor/jquery.mobile.custom.min.js"></script>
	<script type="text/javascript" src="../commun/js/vendor/hTimeline.js"></script>
	<!-- amcharts -->
	<script type="text/javascript" src="../commun/js/vendor/amcharts/amcharts.js"></script>
	<script type="text/javascript" src="../commun/js/vendor/amcharts/serial.js"></script>
	<script type="text/javascript" src="../commun/js/vendor/amcharts/pie.js"></script>
	<script type="text/javascript" src="../commun/js/vendor/amcharts/gantt.js"></script>
	<script type="text/javascript" src="../commun/js/vendor/amcharts/funnel.js"></script>
	<script type="text/javascript" src="../commun/js/vendor/amcharts/radar.js"></script>
	<script type="text/javascript" src="../commun/js/vendor/amcharts/amstock.js"></script>
	<script type="text/javascript" src="../commun/js/vendor/amcharts/ammap.js"></script>
	<script type="text/javascript" src="../commun/js/vendor/amcharts/worldLow.js"></script>
	<script type="text/javascript" src="../commun/js/vendor/amcharts/light.js"></script>
	<!-- Peity -->
	<script type="text/javascript" src="../commun/js/vendor/peityJs/jquery.peity.min.js"></script>
	<!-- fullcalendar -->
	<script type="text/javascript" src="../commun/js/vendor/lib/moment.min.js"></script>
	<script type="text/javascript" src="../commun/js/vendor/lib/jquery-ui.custom.min.js"></script>
	<script type="text/javascript" src="../commun/js/vendor/fullcalendar.min.js"></script>
	<!-- icheck -->
	<script type="text/javascript" src="../commun/js/vendor/icheck.js"></script>
	<!-- dataTables-->
	<script type="text/javascript" src="../commun/js/vendor/jquery.dataTables.js"></script>
	<script type="text/javascript" src="../commun/js/vendor/dataTables.bootstrap.min.js"></script>
	<!-- js for print and download -->
	<script type="text/javascript" src="../commun/js/vendor/dataTables.buttons.min.js"></script>
	<script type="text/javascript" src="../commun/js/vendor/buttons.flash.min.js"></script>
	<script type="text/javascript" src="../commun/js/vendor/jszip.min.js"></script>
	<script type="text/javascript" src="../commun/js/vendor/pdfmake.min.js"></script>
	<script type="text/javascript" src="../commun/js/vendor/vfs_fonts.js"></script>
	<script type="text/javascript" src="../commun/js/vendor/buttons.html5.min.js"></script>
	<script type="text/javascript" src="../commun/js/vendor/buttons.print.min.js"></script>
	<script type="text/javascript" src="../commun/js/vendor/dataTables.responsive.min.js"></script>
	<script type="text/javascript" src="../commun/js/vendor/dataTables.fixedHeader.min.js"></script>
	<!-- slimscroll js -->
	<script type="text/javascript" src="../commun/js/vendor/jquery.slimscroll.js"></script>
	<!-- dashboard1 js -->
	<script type="text/javascript" src="../commun/js/dashboard1.js"></script>
	<!-- pace js -->
	<script type="text/javascript" src="../commun/js/vendor/pace/pace.min.js"></script>
	<!-- main js -->
	<script type="text/javascript" src="../commun/js/main.js"></script>
	<!-- adminbag demo js-->
	<script type="text/javascript" src="../commun/js/adminbagdemo.js"></script>