<html lang="en"><head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Categories | Yanfoma The hotspot of technologie </title>
  <!-- Bootstrap -->
  <link href="assets/css/bootstrap.min.css" rel="stylesheet">
  <!-- slimscroll -->
  <link href="assets/css/jquery.slimscroll.css" rel="stylesheet">
  <!-- project -->
  <link href="assets/css/project.css" rel="stylesheet">
  <!--timeline_horizontal-->
  <link rel="stylesheet" href="assets/css/horizontalTimeLine.css">
  <!-- dataTables -->
  <link href="assets/css/buttons.dataTables.min.css" rel="stylesheet">
  <link href="assets/css/dataTables.bootstrap.min.css" rel="stylesheet">
  <link href="assets/css/responsive.dataTables.min.css" rel="stylesheet">
  <link href="assets/css/fixedHeader.dataTables.min.css" rel="stylesheet">
  <!-- Fontes -->
  <link href="assets/css/font-awesome.min.css" rel="stylesheet">
  <link href="assets/css/simple-line-icons.css" rel="stylesheet">
  <link href="assets/css/ameffectsanimation.css" rel="stylesheet">
  <link href="assets/css/buttons.css" rel="stylesheet">
  <!-- animate css -->
  <link href="assets/css/animate.css" rel="stylesheet">
  <!-- adminbag main css -->
  <link href="assets/css/main.css" rel="stylesheet">
  <!-- fullcalendar -->
  <link href="assets/css/fullcalendar.css" rel="stylesheet">
  <link href="assets/css/fullcalendar.print.css" rel="stylesheet" media="print">
  <!-- icheck -->
  <link href="assets/css/skins/all.css" rel="stylesheet">
  <!-- adminbag demo css-->
  <link href="assets/css/adminbagdemo.css" rel="stylesheet">
  <!-- aqua black theme css -->
  <link href="assets/css/aqua-black.css" rel="stylesheet">
  <!-- media css for responsive  -->
  <link href="assets/css/main.media.css" rel="stylesheet">
  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="page-sidebar-fixed page-sidebar-closed page-footer-fixed  pace-done page-header-fixed"><div class="pace  pace-inactive"><div class="pace-progress" data-progress-text="100%" data-progress="99" style="transform: translate3d(100%, 0px, 0px);">
  <div class="pace-progress-inner"></div>
</div>
<div class="pace-activity"></div></div>
  <div class="pace-progress-inner"></div>

<div class="pace-activity"></div>
  <?php include_once("librairies/header.php");?>
  <div class="clearfix"> </div>
  <div class="page-container">
    <!-- Start page sidebar wrapper -->
    <?php include_once("librairies/menu.php");?>
    <!-- End page sidebar wrapper -->
    <div class="page-content-wrapper animated fadeInRight">
      <div class="page-content">
        <!-- Start DashBoard -->
        <?php include_once("librairies/categories.php");?>
        <!-- End DashBoard -->
        <?php include_once("librairies/footer.php");?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Go top -->
  <a href="index.html#" class="scrollup" style="display: none;"><i class="fa fa-chevron-up"></i></a>
  <!-- Go top -->
  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="assets/js/vendor/jquery.min.js"></script>
  <!-- bootstrap js -->
  <script src="assets/js/vendor/bootstrap.min.js"></script>
  <!--  chartJs js  -->
  <script src="assets/js/vendor/chartJs/Chart.bundle.js"></script>
  <!--timeline_horizontal-->
  <script src="assets/js/vendor/jquery.mobile.custom.min.js"></script>
  <script src="assets/js/vendor/hTimeline.js"></script>
  <!-- amcharts -->
  <script src="assets/js/vendor/amcharts/amcharts.js"></script>
  <script src="assets/js/vendor/amcharts/serial.js"></script>
  <script src="assets/js/vendor/amcharts/pie.js"></script>
  <script src="assets/js/vendor/amcharts/gantt.js"></script>
  <script src="assets/js/vendor/amcharts/funnel.js"></script>
  <script src="assets/js/vendor/amcharts/radar.js"></script>
  <script src="assets/js/vendor/amcharts/amstock.js"></script>
  <script src="assets/js/vendor/amcharts/ammap.js"></script>
  <script src="assets/js/vendor/amcharts/worldLow.js"></script>
  <script src="assets/js/vendor/amcharts/light.js"></script>
  <!-- Peity -->
  <script src="assets/js/vendor/peityJs/jquery.peity.min.js"></script>
  <!-- fullcalendar -->
  <script src="assets/js/vendor/lib/moment.min.js"></script>
  <script src="assets/js/vendor/lib/jquery-ui.custom.min.js"></script>
  <script src="assets/js/vendor/fullcalendar.min.js"></script>
  <!-- icheck -->
  <script src="assets/js/vendor/icheck.js"></script>
  <!-- dataTables-->
  <script type="text/javascript" src="assets/js/vendor/jquery.dataTables.js"></script>
  <script type="text/javascript" src="assets/js/vendor/dataTables.bootstrap.min.js"></script>
  <!-- js for print and download -->
  <script type="text/javascript" src="assets/js/vendor/dataTables.buttons.min.js"></script>
  <script type="text/javascript" src="assets/js/vendor/buttons.flash.min.js"></script>
  <script type="text/javascript" src="assets/js/vendor/jszip.min.js"></script>
  <script type="text/javascript" src="assets/js/vendor/pdfmake.min.js"></script>
  <script type="text/javascript" src="assets/js/vendor/vfs_fonts.js"></script>
  <script type="text/javascript" src="assets/js/vendor/buttons.html5.min.js"></script>
  <script type="text/javascript" src="assets/js/vendor/buttons.print.min.js"></script>
  <script type="text/javascript" src="assets/js/vendor/dataTables.responsive.min.js"></script>
  <script type="text/javascript" src="assets/js/vendor/dataTables.fixedHeader.min.js"></script>
  <!-- slimscroll js -->
  <script type="text/javascript" src="assets/js/vendor/jquery.slimscroll.js"></script>
  <!-- dashboard1 js -->
  <script src="assets/js/dashboard1.js"></script>
  <!-- pace js -->
  <script src="assets/js/vendor/pace/pace.min.js"></script>
  <!-- main js -->
  <script src="assets/js/main.js"></script>
  <!-- adminbag demo js-->
  <script src="assets/js/adminbagdemo.js"></script>
</body></html>