<?php
 session_start();
 include("../commun/config.php");
 include("../commun/db.php");
 if( isset($_GET['entity']) && isset($_GET['action']) && isset($_GET['id']) ){
      $entity = mysqli_real_escape_string($db,$_GET['entity']);
      $action = mysqli_real_escape_string($db,$_GET['action']);
      $id     = mysqli_real_escape_string($db,$_GET['id']);

      if($action=="delete"){
        if($entity=="creer"){
            $query= "DELETE FROM admin WHERE id='$id'";
            $db->query($query);
            header("Location:creer.php?del_succ_msg=User successfully Deleted!!!");
			exit();
        }else{
        	 header("Location:creer.php?del_err_msg=An Error Occur. Please Try Again!!!");
			exit();
        }
    }
  }
 $query = "SELECT * FROM admin";
 $user  = $db->query($query);
 if(isset($_POST['add'])){

  $name     = mysqli_real_escape_string($db, $_POST['name']);
 	$email    = mysqli_real_escape_string($db, $_POST['email']);
 	$username = mysqli_real_escape_string($db, $_POST['username']);
 	$password = mysqli_real_escape_string($db, $_POST['password']);

 	if(empty($email) | empty($username) |empty($password)){
 		$error = "Please All the fields are required";
 	}
 	if(!(empty($email)) && !(empty($username)) && !(empty($password))){
 		$query2  = "SELECT * FROM admin WHERE email = '$email'";
 		$result2 = $db->query($query2);
 		if($result2->num_rows == 1 ){
			header("Location:creer.php?err_msg=This Email Address Already Exists. Please Try Again!!!");
			exit();
 		}else{
    $password = md5($password);
 		$query  = "INSERT INTO admin(name,email,username,password) VALUES('$name','$email','$username','$password')";
 		$result = $db->query($query);
            header("Location:creer.php?succ_msg=User Added successfully!!!");
			exit();
		}

 	}
 }

?>
<html lang="en"><head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>CREATE USER | Afrik Eveil</title>
  <?php include_once("librairies/style.php");?>
  <style type="text/css">
  	.alert{
  		position: relative;
  		width: 100%;
  		right: 0%;
  		top: 0px;
  		z-index: 10000;
  	}
  </style>
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="page-sidebar-fixed page-sidebar-closed page-footer-fixed  pace-done page-header-fixed">
	<div class="pace  pace-inactive">
		<div class="pace-progress" data-progress-text="100%" data-progress="99" style="transform: translate3d(100%, 0px, 0px);">
  			<div class="pace-progress-inner"></div>
		</div>
		<div class="pace-activity"></div>
	</div>
    <div class="pace-progress-inner"></div>
	<?php include_once("librairies/header.php");?>
	<!-- Start page content wrapper -->
		<?php include_once("librairies/menu.php");?>
<div class="page-content-wrapper animated fadeInRight">
  <div class="page-content" >
    <div class="row wrapper border-bottom page-heading">
    </div>
    <div class="wrapper-content ">
      <div class="row">
        <!-- Basic Form start -->
        <div class="col-lg-6">
          <div class="ibox float-e-margins">
            <div class="widgets-container">
              <h5>ADD A USER</h5>
              <hr>
              <span  style="color:red; font-weight: bold; font-size: 16px;"><?php if(isset($_GET['err_msg'])) echo $_GET['err_msg']; ?></span>
              <span  style="color:green; font-weight: bold; font-size: 16px;"><?php if(isset($_GET['succ_msg'])) echo $_GET['succ_msg']; ?></span>
              <form method="post" class="top15">
        				<div class="form-group">
        					<label>Name</label>
        					<input type="name" placeholder="name" 		name="name" class="form-control">
        					<span  style="color:red;"><?php echo($errorEmail);?></span>
        				</div>
                <div class="form-group">
                  <label>Email</label>
                  <input type="email" placeholder="email"     name="email" class="form-control">
                  <span  style="color:red;"><?php echo($errorEmail);?></span>
                </div>
        				<div class="form-group">
        					<label>Username</label>
        					<input type="text" placeholder="Username" 		 name="username" class="form-control">
        					<span  style="color:red;"><?php echo($errorUsername);?></span>
        				</div>
        				<div class="form-group">
        					<label>Password</label>
        					<input type="password" placeholder="Password" 	 name="password" class="form-control">
        					<span  style="color:red;"><?php echo($errorPassword);?></span>
        				</div>
        				<button class="btn green block full-width bottom15" type="submit" name="add">Create</button>
			       </form>
            </div>
          </div>
        </div>
        <div class="col-lg-6">
            <div class="widgets-container">
              <h5>ALL THE USERS </h5>
              <div id="demo6" class="ibox-content collapse in">
                  <div class="table-scrollable">

              <span  style="color:red; font-weight: bold; font-size: 16px;"><?php if(isset($_GET['del_err_msg'])) echo $_GET['del_err_msg']; ?></span><br>
              <span  style="color:green; font-weight: bold; font-size: 16px;"><?php if(isset($_GET['del_succ_msg'])) echo $_GET['del_succ_msg']; ?></span>
                    <table class="table table-bordered table-hover">
                      <thead>
                        <tr>
                          <th> Name </th>
                          <th> Username </th>
                          <th> Email</th>
                          <th> Action </th>
                        </tr>
                      </thead>
                      <tbody>
                      	<?php
                  if($user->num_rows > 0){
                    while($row = $user->fetch_assoc() ) { ?>
                      <tr>
                        <td><?php echo $row['name'] ?></td>
                        <td><?php echo $row['username'] ?></td>
                        <td><?php echo $row['email'] ?></td>
                        <td>
                          <a class="btn btn-danger"href="creer.php?entity=creer&action=delete&id=<?php echo $row['id'] ?>"> <i class="fa fa-times"></i> Delete </a>
                        </td>
                      </tr>
                    <?php }} ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
        </div>
        <!-- Basic Form End -->
	<!-- Go top -->
	<a href="index.html#" class="scrollup" style="display: none;"><i class="fa fa-chevron-up"></i></a>
	<?php include_once("librairies/script.php");?>
</body>
</html>