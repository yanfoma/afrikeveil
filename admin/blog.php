<html lang="en"><head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>BLOG | Afrik Eveil</title>
  <?php include_once("librairies/style.php");?>
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="page-sidebar-fixed page-sidebar-closed page-footer-fixed  pace-done page-header-fixed">
	<div class="pace  pace-inactive">
		<div class="pace-progress" data-progress-text="100%" data-progress="99" style="transform: translate3d(100%, 0px, 0px);">
  			<div class="pace-progress-inner"></div>
		</div>
		<div class="pace-activity"></div>
	</div>
    <div class="pace-progress-inner"></div>
	<?php include_once("librairies/header.php");?>
	<div class="clearfix"> </div>
	<div class="page-container">
		<!-- Start page sidebar wrapper -->
		<?php include_once("librairies/menu.php");?>
		<!-- End page sidebar wrapper -->
		<div class="page-content-wrapper animated fadeInRight">
			<div class="page-content">
				<!-- Start DashBoard -->
				<?php include_once("librairies/blog.php");?>
				<!-- End DashBoard -->
				<?php include_once("librairies/footer.php");?>
			</div>
		</div>
	</div>
	<!-- Go top -->
	<a href="index.html#" class="scrollup" style="display: none;"><i class="fa fa-chevron-up"></i></a>
	<?php include_once("librairies/script.php");?>
</body>
</html>