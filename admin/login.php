<?php
 session_start();
 include("../commun/config.php");
 include("../commun/db.php");
 $error = "";

 if(isset($_POST['login'])){

 	$email    = mysqli_real_escape_string($db, $_POST['email']);
 	$username = mysqli_real_escape_string($db, $_POST['username']);
 	$password = mysqli_real_escape_string($db, $_POST['password']);

 	/*echo "Your email    is $email".'<br>';
 	echo "Your username is $username".'<br>';
 	echo "Your password is $password".'<br>';*/

 	if(empty($email) | empty($username) |empty($password)){
 		$error = "Please All the fields are required";
 	}
 	if(!(empty($email)) && !(empty($username)) && !(empty($password))){
 		//$password = md5($password);
 		$query  = "SELECT * FROM admin WHERE email = '$email' AND username = '$username' AND password = '$password'";
 		$result = $db->query($query);
 		/*while($row 	= $result->fetch_assoc()){
 			echo $row['email'].'<br>';echo $row['username'].'<br>';echo $row['password'].'<br>';
 		}*/
 		if($result->num_rows == 1 ){
 			//echo "row num is $result->num_rows".'<br>';
			$_SESSION['email']    = $email;
			$_SESSION['username'] = $username;
			header("Location:index.php");
			exit();
 		} else{
 			header("location:login.php?err_msg=Wrong Credentials Try again!!!");
 			exit();
 		}
 	}
 }

?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title> Admin Board | AFRIK EVEIL  </title>
	<?php include_once("librairies/style.php");?>
</head>

<body class="green-bg login">
	<div class="logo">
		<a href="index.php"> <img src="../commun/images/admin/logo2.png" alt=""> </a>
	</div>
	<div class="middle-box text-center loginscreen ">
		<div class="widgets-container">
			<div class="bottom20"> <img alt="image" class="img-circle circle-border" src="../commun/images/admin/admin.png"> </div>
			<p>Afrik Eveil Admin Board</p>
			<span  style="color:red;"><?php if(isset($_GET['err_msg'])) echo $_GET['err_msg']; ?></span>
			<form method="post" class="top15">
				<div class="form-group">
					<label>Email</label>
					<input type="email" placeholder="email" 		name="email" class="form-control">
					<span  style="color:red;"><?php echo($errorEmail);?></span>
				</div>
				<div class="form-group">
					<label>Username</label>
					<input type="text" placeholder="Username" 		 name="username" class="form-control">
					<span  style="color:red;"><?php echo($errorUsername);?></span>
				</div>
				<div class="form-group">
					<label>Password</label>
					<input type="password" placeholder="Password" 	 name="password" class="form-control">
					<span  style="color:red;"><?php echo($errorPassword);?></span>
				</div>
				<button class="btn green block full-width bottom15" type="submit" name="login">Login</button>
			</form>
		</div>
	</div>
</body>

</html>